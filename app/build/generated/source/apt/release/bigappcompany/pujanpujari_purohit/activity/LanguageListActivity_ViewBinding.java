// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class LanguageListActivity_ViewBinding implements Unbinder {
  private LanguageListActivity target;

  private View view2131296323;

  @UiThread
  public LanguageListActivity_ViewBinding(LanguageListActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public LanguageListActivity_ViewBinding(final LanguageListActivity target, View source) {
    this.target = target;

    View view;
    target.common_recyclerview = Utils.findRequiredViewAsType(source, R.id.common_recyclerview, "field 'common_recyclerview'", RecyclerView.class);
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.mToolbar, "field 'mToolbar'", Toolbar.class);
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'doneClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.doneClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    LanguageListActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.common_recyclerview = null;
    target.mToolbar = null;
    target.button_submit = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
  }
}
