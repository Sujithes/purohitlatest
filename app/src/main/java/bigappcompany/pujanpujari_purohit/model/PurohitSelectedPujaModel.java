package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 9/3/18.
 */

public class PurohitSelectedPujaModel {

    public String getPuja_name() {
        return puja_name;
    }

    public void setPuja_name(String puja_name) {
        this.puja_name = puja_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPuja_image() {
        return puja_image;
    }

    public void setPuja_image(String puja_image) {
        this.puja_image = puja_image;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    @SerializedName("puja_name")
    String puja_name;
    @SerializedName("id")
    String id;
    @SerializedName("puja_image")
    String puja_image;
    @SerializedName("short_description")
    String short_description;
}
