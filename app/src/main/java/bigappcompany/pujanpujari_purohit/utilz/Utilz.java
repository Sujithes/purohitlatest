package bigappcompany.pujanpujari_purohit.utilz;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Date;

import es.dmoral.toasty.Toasty;

/**
 * Created by shankar on 27/12/17.
 */

public class Utilz {

    public static Boolean isInternetAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return true;

            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return true;
        }
        return false;
    }

    public static void displayMessageAlert(String Message, Context context) {
        try {
            new AlertDialog.Builder(context).setMessage(Message).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface arg0, int arg1) {

                }
            }).create().show();
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public static boolean validateField(Context context, String text)
    {
        if (text!=null && text.trim().length()>0) return  true;

        return false;
    }

    public static boolean isValidEmail1(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
                    .matches();
        }
    }


    public static void HideSoftkeyPad(EditText textfiled, Context c) {
        InputMethodManager imm = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(textfiled.getWindowToken(), 0);
    }


    static ProgressDialog dialog;
    public static void showProgress(Context context, String message) {
        if (dialog != null && dialog.isShowing()) {
            return;
        }
        dialog = ProgressDialog.show(context, null, message, true, true);
    }

    public static void dismissProgress() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }





    public static void amazingMessage(Context context, String message, int type)
    {
      switch (type)
      {
          case PreferenceName.ERROR_MESSAGE:
              Toasty.error(context, message, Toast.LENGTH_LONG, true).show();
              break;
          case PreferenceName.SUCCESS_MESSAGE:
              Toasty.success(context, message, Toast.LENGTH_LONG, true).show();
              break;
          case PreferenceName.INFO_MESSAGE:
              Toasty.info(context, message, Toast.LENGTH_LONG, true).show();
              break;
          case PreferenceName.WARNING_MESSAGE:
              Toasty.warning(context, message, Toast.LENGTH_LONG, true).show();
              break;
          case PreferenceName.NORMAL_MESSAGE:
              Toasty.normal(context, message).show();
              break;
          /*case PreferenceName.NORMAL_ICON_MESSAGE:
              Toasty.normal(context, message, yourIconDrawable).show();
              break;*/
          default:
              break;
      }
    }

    public static String getNumber(String phoneNumber) {
            String number = phoneNumber;
            if (phoneNumber.contains("+"))
            {
                number = phoneNumber.replace("+","");
            }
            return number.trim();
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int pixels) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap
                .getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);
        final float roundPx = pixels;

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }

    public static String getFromdate(String from_time) {
        String fromtime = "";
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(from_time);
          //  System.out.println(_24HourDt);
          //  System.out.println(_12HourSDF.format(_24HourDt));
            fromtime = _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return fromtime;
    }

    public static String getToTime(String to_time) {
        String totime = "";
        try {
            SimpleDateFormat _24HourSDF = new SimpleDateFormat("HH:mm");
            SimpleDateFormat _12HourSDF = new SimpleDateFormat("hh:mm a");
            Date _24HourDt = _24HourSDF.parse(to_time);
            totime = _12HourSDF.format(_24HourDt);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return totime;
    }

    public static String dateFormat(String booking_date, int i) {
        String upToNCharacters = booking_date.substring(0, Math.min(booking_date.length(), i));

        return upToNCharacters;
    }
}
