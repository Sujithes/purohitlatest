package bigappcompany.pujanpujari_purohit.utilz;

/**
 * Created by shankar on 27/12/17.
 */

public class PreferenceName {

    public static final String USERROLE = "purohit";
    public static final int ERROR_MESSAGE = 100;
    public static final int SUCCESS_MESSAGE = 101;
    public static final int INFO_MESSAGE = 102;
    public static final int WARNING_MESSAGE = 103;
    public static final int NORMAL_MESSAGE = 104;
    public static final int NORMAL_ICON_MESSAGE = 105;
    public static final int CUSTOM_MESSAGE = 106;
    public static final String APIKEY = "x-api-key";
    public static final String AUTHORIZATION = "Authorization";
        public static final String SESSION_TOKEN_HEADER = "session-token";
    public static final String TRUE = "true";
    public static final String USER_MOBILE = "mobile";
    public static final String USER_TOKEN = "token";
    public static final String USER_ID = "id";
    public static final String USER_EMAIL = "email";
    public static final String USER_SESSION_TOKEN = "sessiontoken";
    public static final String USER_NAME = "name";
    public static final String USER_ADDRESS = "address";
    public static final String USER_IMAGE = "image";
    public static final String SEEKRANGE = "seek_range";
    public static final String LATITUTE = "latitute";
    public static final String LONGITUTE = "longitute";
    public static final String CATEGORYNAME = "catname";
    public static final String CITYID = "city_id";
    public static final String DEVICE_TOKEN = "device_token";
}
