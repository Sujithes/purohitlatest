package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 12/3/18.
 */

public class TimeSlotAvailableModel {

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<AvailabilityDetails> getAvailability_details() {
        return availability_details;
    }

    public void setAvailability_details(ArrayList<AvailabilityDetails> availability_details) {
        this.availability_details = availability_details;
    }

    @SerializedName("user_id")
    String user_id;
    @SerializedName("date")
    String date;
    @SerializedName("id")
    String id;
    @SerializedName("availability_details")
    ArrayList<AvailabilityDetails> availability_details;
}
