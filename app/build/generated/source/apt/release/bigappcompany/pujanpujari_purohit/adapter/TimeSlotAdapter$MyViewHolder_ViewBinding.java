// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class TimeSlotAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private TimeSlotAdapter.MyViewHolder target;

  @UiThread
  public TimeSlotAdapter$MyViewHolder_ViewBinding(TimeSlotAdapter.MyViewHolder target,
      View source) {
    this.target = target;

    target.tv_timeslot = Utils.findRequiredViewAsType(source, R.id.tv_timeslot, "field 'tv_timeslot'", TextView.class);
    target.ll_timeslot = Utils.findRequiredViewAsType(source, R.id.ll_timeslot, "field 'll_timeslot'", LinearLayout.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    TimeSlotAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tv_timeslot = null;
    target.ll_timeslot = null;
  }
}
