package bigappcompany.pujanpujari_purohit.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;

import java.util.ArrayList;
import java.util.List;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.fragment.BookedFragment;
import bigappcompany.pujanpujari_purohit.fragment.CompletedFragment;
import bigappcompany.pujanpujari_purohit.fragment.InProgressFragment;
import bigappcompany.pujanpujari_purohit.fragment.OnMyWayFragment;
import bigappcompany.pujanpujari_purohit.fragment.Today;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 8/2/18.
 */

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.iv_booking)
    ImageView iv_booking;
    @BindView(R.id.tv_booking)
    TextView tv_booking;
    @BindView(R.id.iv_callendar)
    ImageView iv_callendar;
    @BindView(R.id.tv_callendar)
    TextView tv_callendar;
    @BindView(R.id.iv_profile)
    ImageView iv_profile;
    @BindView(R.id.tv_profile)
    TextView tv_profile;
    @BindView(R.id.ll_booking)
    LinearLayout ll_booking;
    @BindView(R.id.ll_callenda)
    LinearLayout ll_callenda;
    @BindView(R.id.ll_profile)
    LinearLayout ll_profile;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        initWidgit();
        tabclickListener();
        try {
            int pos = getIntent().getIntExtra("pos", 0);
            goToFragment(pos);
        } catch (Exception e) {
            goToFragment(0);
        }

        showDialogNotification(this);

    }

    private void showDialogNotification(Context context) {
        String notifmsg=getIntent().getStringExtra("notifmsg");

        if(notifmsg != null){
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Alert")
                    .setMessage("Please accept request within a minute")
                    .setCancelable(true)
                    .setNeutralButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            Fragment fragment = new Today();
                        }
                    });
            builder.create().show();
            builder.setCancelable(false);
        }
        else{

        }

    }

    private void goToFragment(int i) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setCurrentItem(i);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick({R.id.ll_booking, R.id.ll_callenda, R.id.ll_profile})
    void onLinearLayoutClick(LinearLayout linearLayout) {
        if (linearLayout.getId() == R.id.ll_booking) {
            changetextNimage(iv_booking, tv_booking, 1);
        } else if (linearLayout.getId() == R.id.ll_callenda) {
            changetextNimage(iv_callendar, tv_callendar, 2);
        } else {
            changetextNimage(iv_profile, tv_profile, 3);
        }
    }

    private void changetextNimage(ImageView bookingImage, TextView bookingText, int numbe) {
        tv_booking.setTextColor(getResources().getColor(R.color.textColorSecondary));
        tv_callendar.setTextColor(getResources().getColor(R.color.textColorSecondary));
        tv_profile.setTextColor(getResources().getColor(R.color.textColorSecondary));

        iv_booking.setBackgroundResource(R.mipmap.mybooking);
        iv_callendar.setBackgroundResource(R.mipmap.calendar);
        iv_profile.setBackgroundResource(R.mipmap.profile);

        if (numbe == 1) {
            bookingImage.setBackgroundResource(R.mipmap.mybooking_color);
            goToPageOne();
        } else if (numbe == 2) {
            bookingImage.setBackgroundResource(R.mipmap.calendar_color);
            gotoPageTwo();
        } else {
            bookingImage.setBackgroundResource(R.mipmap.profile_color);
            gotoPageThree();
        }
        bookingText.setTextColor(getResources().getColor(R.color.colorAccent));
    }

    private void gotoPageThree() {
        startActivity(new Intent(MainActivity.this, Profile.class));
    }

    private void gotoPageTwo() {
        startActivity(new Intent(MainActivity.this, CalendarActivity.class));
    }

    private void goToPageOne() {
        initWidgit();
        tabclickListener();
    }

    private void initWidgit() {
        setSupportActionBar(mToolbar);
        mToolbar.setNavigationIcon(R.mipmap.navigation);


    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();//fragment arraylist
        private final List<String> mFragmentTitleList = new ArrayList<>();//title arraylist

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new Today();
            } else if (position == 1) {
                fragment = new InProgressFragment();
            } else if (position == 2) {
                fragment = new BookedFragment();
            } else if (position == 3) {
                fragment = new OnMyWayFragment();
            } else if (position == 4) {
                fragment = new CompletedFragment();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 5;
        }


        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            if (position == 0) {
                title = "New";
            } else if (position == 1) {
                title = "Accepted";
            } else if (position == 2) {
                title = "Booked";
            } else if (position == 3) {
                title = "OnMyWay";
            } else if (position == 4) {
                title = "Completed";
            }
            return title;
        }
    }

    private void tabclickListener() {


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            tabLayout.setupWithViewPager(pager);//setting tab over viewpager
        }

        //Implementing tab selected listener over tablayout
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                pager.setCurrentItem(tab.getPosition());//setting current selected item over viewpager
                switch (tab.getPosition()) {
                    case 0:
                        break;
                    case 1:
                        break;

                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                //   tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.textColorPrimary)));

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
//                tabLayout.setTabTextColors(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        changetextNimage(iv_booking, tv_booking, 1);
    }
}
