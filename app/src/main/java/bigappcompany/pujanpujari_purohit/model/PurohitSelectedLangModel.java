package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 9/3/18.
 */

public class PurohitSelectedLangModel {
    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @SerializedName("language_name")
    String language_name;
    @SerializedName("id")
    String id;
}
