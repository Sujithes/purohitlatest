package bigappcompany.pujanpujari_purohit.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.allinterface.OnCheckStatusCode;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 29/12/17.
 */

public class BaseActivity extends AppCompatActivity {
    static ProgressDialog dialog;
    int onStartCount = 0;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        animateView(savedInstanceState);
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    private void animateView(Bundle savedInstanceState) {
        onStartCount = 1;
        if (savedInstanceState == null) // 1st time
        {
            this.overridePendingTransition(R.anim.anim_slide_in_left,
                    R.anim.anim_slide_out_left);
        } else // already created so reverse animation
        {
            onStartCount = 2;
        }
    }

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
        if (onStartCount > 1) {
            this.overridePendingTransition(R.anim.anim_slide_in_right,
                    R.anim.anim_slide_out_right);

        } else if (onStartCount == 1) {
            onStartCount++;
        }

    }

    public static void showDailog(Context c, String msg) {
        dialog = new ProgressDialog(c);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setMessage(msg);
        dialog.show();
    }

    public static void closeDialog() {
        if (dialog != null)
            dialog.cancel();
    }


    public  void checkStatusCode(Context context , int statuscode, String message, OnCheckStatusCode onCheckStatusCode)
    {
        if (statuscode == 200)
        {
            onCheckStatusCode.statuscode(statuscode);
        }
        else if(statuscode == 400)
        {


            Utilz.amazingMessage(context,context.getResources().getString(R.string.responsecode_400), PreferenceName.ERROR_MESSAGE);
            onCheckStatusCode.statuscode(statuscode);
        }

        else if(statuscode == 404)
        {
            Utilz.amazingMessage(context,context.getResources().getString(R.string.responsecode_404), PreferenceName.ERROR_MESSAGE);
            onCheckStatusCode.statuscode(statuscode);
        }
        else if(statuscode == 500)
        {
            Utilz.amazingMessage(context,context.getResources().getString(R.string.responsecode_500), PreferenceName.ERROR_MESSAGE);
            onCheckStatusCode.statuscode(statuscode);
        }

    }
}
