package bigappcompany.pujanpujari_purohit.retrofitnetwork;


import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import bigappcompany.pujanpujari_purohit.activity.BaseActivity;
import bigappcompany.pujanpujari_purohit.allinterface.OnCheckStatusCode;
import bigappcompany.pujanpujari_purohit.common.ConsantValue;
import bigappcompany.pujanpujari_purohit.model.LanguageListModel;
import bigappcompany.pujanpujari_purohit.model.PujaListModel;
import bigappcompany.pujanpujari_purohit.model.PurohitDetailsModel;
import bigappcompany.pujanpujari_purohit.model.PurohitSlotAvalabilityModel;
import bigappcompany.pujanpujari_purohit.model.SignUpModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.model.TimeSlotModel;
import bigappcompany.pujanpujari_purohit.model.UserBookingListModel;
import bigappcompany.pujanpujari_purohit.model.UserDetailsModel;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitDataProvider extends BaseActivity implements ServiceMethods {
    private Context context;

    private ApiRetrofitService createRetrofitService() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ConsantValue.BASE_URL)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(ApiRetrofitService.class);

    }

    public RetrofitDataProvider(Context context) {
        this.context = context;
    }


    @Override
    public void login(String mobileNo, String role, final DownlodableCallback<UserDetailsModel> callback) {
        createRetrofitService().otpLogin(ConsantValue.API_KEY, mobileNo, role).enqueue(
                new Callback<UserDetailsModel>() {
                    @Override
                    public void onResponse(@NonNull Call<UserDetailsModel> call, @NonNull final Response<UserDetailsModel> response) {
                        if (response.isSuccessful()) {

                            UserDetailsModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserDetailsModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );

    }

    @Override
    public void verifyOtp(String mobile_number, String otp, String role, String device_token, final DownlodableCallback<UserDetailsModel> callback) {
        createRetrofitService().otpVerification(ConsantValue.API_KEY, mobile_number, otp, role, device_token).enqueue(
                new Callback<UserDetailsModel>() {
                    @Override
                    public void onResponse(@NonNull Call<UserDetailsModel> call, @NonNull final Response<UserDetailsModel> response) {
                        if (response.isSuccessful()) {

                            UserDetailsModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }


                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserDetailsModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void languageList(String headertoken, String authorization, final DownlodableCallback<LanguageListModel> callback) {
        createRetrofitService().getallLanguage(headertoken, authorization).enqueue(
                new Callback<LanguageListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<LanguageListModel> call, @NonNull final Response<LanguageListModel> response) {
                        if (response.isSuccessful()) {

                            LanguageListModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<LanguageListModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void signupPurohit(String first_name, String last_name, String role, String email, String mobile_number, String address, String latitude, String longitude, String area_of_service, final DownlodableCallback<SignUpModel> callback) {
        createRetrofitService().signUp(first_name, last_name, role, email, mobile_number, address, latitude, longitude, area_of_service).enqueue(
                new Callback<SignUpModel>() {
                    @Override
                    public void onResponse(@NonNull Call<SignUpModel> call, @NonNull final Response<SignUpModel> response) {
                        if (response.isSuccessful()) {

                            SignUpModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    JSONObject jObjError = new JSONObject(response.errorBody().string());
                                    if (jObjError.optString("apiStatus").equals("false")) {
                                        JSONObject object = jObjError.getJSONObject("result");
                                        String errormgs = object.getJSONObject("invalidAttributes").getJSONArray("email").getJSONObject(0).getString("message");
                                        callback.onFailure(errormgs);
                                    }
                                } catch (Exception e) {

                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<SignUpModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void saveLanguage(String headertoken, String authorization, int[] language_id, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().addlanguage(headertoken, authorization, language_id).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void pujaListForPurohit(String headertoken, String authorization, final DownlodableCallback<PujaListModel> callback) {
        createRetrofitService().getPujaList(headertoken, authorization).enqueue(
                new Callback<PujaListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<PujaListModel> call, @NonNull final Response<PujaListModel> response) {
                        if (response.isSuccessful()) {

                            PujaListModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PujaListModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void savePujaService(String headertoken, String authorization, int[] puja_id, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().addPujaService(headertoken, authorization, puja_id).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void purohitDetails(String headertoken, String authorization, final DownlodableCallback<PurohitDetailsModel> callback) {
        createRetrofitService().getPurohitDetails(headertoken, authorization).enqueue(
                new Callback<PurohitDetailsModel>() {
                    @Override
                    public void onResponse(@NonNull Call<PurohitDetailsModel> call, @NonNull final Response<PurohitDetailsModel> response) {
                        if (response.isSuccessful()) {

                            PurohitDetailsModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PurohitDetailsModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void purohitTimeSlot(String headertoken, String authorization, final DownlodableCallback<TimeSlotModel> callback) {
        createRetrofitService().purohitTimeSlot(headertoken, authorization).enqueue(
                new Callback<TimeSlotModel>() {
                    @Override
                    public void onResponse(@NonNull Call<TimeSlotModel> call, @NonNull final Response<TimeSlotModel> response) {
                        if (response.isSuccessful()) {

                            TimeSlotModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<TimeSlotModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void purohitTimeAvailable(String headertoken, String authorization, String date, final DownlodableCallback<PurohitSlotAvalabilityModel> callback) {
        createRetrofitService().slotAvailable(headertoken, authorization, date).enqueue(
                new Callback<PurohitSlotAvalabilityModel>() {
                    @Override
                    public void onResponse(@NonNull Call<PurohitSlotAvalabilityModel> call, @NonNull final Response<PurohitSlotAvalabilityModel> response) {
                        if (response.isSuccessful()) {

                            PurohitSlotAvalabilityModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<PurohitSlotAvalabilityModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void saveTimeSlot(String headertoken, String authorization, int[] timeslot_id, String date, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().addTimeslot(headertoken, authorization, timeslot_id, date).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void saveServiceDistance(String headertoken, String authorization, String area_of_service, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().addpurohitDetails(headertoken, authorization, area_of_service).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void acceptBooking(String headertoken, String authorization, String request_id, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().acceptBooking(headertoken, authorization, request_id).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void rejectBooking(String headertoken, String authorization, String request_id, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().rejectBooking(headertoken, authorization, request_id).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void userBookinList(String headertoken, String authorization, String request_status, final DownlodableCallback<UserBookingListModel> callback) {
        createRetrofitService().userBookingList(headertoken, authorization, request_status).enqueue(
                new Callback<UserBookingListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<UserBookingListModel> call, @NonNull final Response<UserBookingListModel> response) {
                        if (response.isSuccessful()) {

                            UserBookingListModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onFailure("login");
                                //callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserBookingListModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void userBookedToCompleted(String headertoken, String authorization, String booking_status, final DownlodableCallback<UserBookingListModel> callback) {
        createRetrofitService().userBookedToCompleted(headertoken, authorization, booking_status).enqueue(
                new Callback<UserBookingListModel>() {
                    @Override
                    public void onResponse(@NonNull Call<UserBookingListModel> call, @NonNull final Response<UserBookingListModel> response) {
                        if (response.isSuccessful()) {

                            UserBookingListModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onFailure("login");
                                //callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<UserBookingListModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void updateBooking(String headertoken, String authorization, String booking_id, String booking_status, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().updateBooking(headertoken, authorization, booking_id, booking_status).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    @Override
    public void updatePurohitAddressLocation(String headertoken, String authorization, String address, String latitude, String longitude, final DownlodableCallback<StatusResultModel> callback) {
        createRetrofitService().addpurohitAddressDetails(headertoken, authorization, address, latitude, longitude).enqueue(
                new Callback<StatusResultModel>() {
                    @Override
                    public void onResponse(@NonNull Call<StatusResultModel> call, @NonNull final Response<StatusResultModel> response) {
                        if (response.isSuccessful()) {

                            StatusResultModel mobileRegisterPojo = response.body();
                            callback.onSuccess(mobileRegisterPojo);

                        } else

                        {
                            if (response.code() == 401) {
                                callback.onUnauthorized(response.code());
                            } else if (response.code() == 400) {
                                try {
                                    getErrorCode(response.errorBody().string());
                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                checkStatusCode(context, response.code(), response.message(), new OnCheckStatusCode() {
                                    @Override
                                    public void statuscode(int code) {
                                        callback.onFailure("");
                                    }
                                });
                            }
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<StatusResultModel> call, @NonNull Throwable t) {
                        Log.d("Result", "t" + t.getMessage());
                        callback.onFailure(t.getMessage());

                    }
                }
        );
    }

    private void getErrorCode(String string) {
        try {
            JSONObject jObjError = new JSONObject(string);
            if (jObjError.optString("apiStatus").equals("false")) {
                JSONObject object = jObjError.getJSONObject("result");
                String errormgs = object.getJSONObject("invalidAttributes").getJSONArray("email").getJSONObject(0).getString("message");
            }
        } catch (Exception e) {

        }
    }

}
