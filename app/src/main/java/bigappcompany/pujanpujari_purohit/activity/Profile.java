package bigappcompany.pujanpujari_purohit.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.LanguageListModel;
import bigappcompany.pujanpujari_purohit.model.LanguageResult;
import bigappcompany.pujanpujari_purohit.model.PujaListModel;
import bigappcompany.pujanpujari_purohit.model.PujaListResultModel;
import bigappcompany.pujanpujari_purohit.model.PurohitDetailsModel;
import bigappcompany.pujanpujari_purohit.model.PurohitSelectedLangModel;
import bigappcompany.pujanpujari_purohit.model.PurohitSelectedPujaModel;
import bigappcompany.pujanpujari_purohit.model.PurohitUserModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;


/**
 * Created by shankar on 12/2/18.
 */

public class Profile extends AppCompatActivity implements OnMapReadyCallback {

    private static final String TAG = "MapActivity";
    @BindView(R.id.tv_locationEdit)
    TextView tv_locationEdit;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.purohitName)
    TextView purohitName;
    @BindView(R.id.purohitLanguage)
    TextView purohitLanguage;
    @BindView(R.id.userMobile)
    TextView userMobile;
    @BindView(R.id.purohitEmail)
    TextView purohitEmail;
    @BindView(R.id.purohitPujaServices)
    TextView purohitPujaServices;
    @BindView(R.id.button_submit)
    Button button_submit;


    private Circle circle = null;
    private LatLng latLng;
    private GoogleMap mMap;

    private RetrofitDataProvider retrofitDataProvider;
    private ArrayList<LanguageResult> langlistModels = new ArrayList<>();
    private ArrayList<String> langid = new ArrayList<>();
    private int all_id[];

    private ArrayList<PujaListResultModel> pujalistModels = new ArrayList<>();
    private ArrayList<String> pujaid = new ArrayList<>();
    private int all_pujaid[];


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );


        retrofitDataProvider = new RetrofitDataProvider(this);
       initWidgit();
       getPurohitDetails();
    }

    @OnClick({R.id.purohitPujaServices, R.id.purohitLanguage})
    void TextClicked(TextView textView){
        if (textView.getId() == R.id.purohitPujaServices){
            getAllPujaservices();
        }
        else if (textView.getId() == R.id.purohitLanguage){
            getAllLanguage();
        }
    }

    @OnClick(R.id.button_submit)
    void onButtonClick(){
        ClsGeneral.setPreferences(Profile.this, PreferenceName.USER_ID,"");
        startActivity(new Intent(Profile.this, Login.class));
        finishAffinity();
    }
    private void getAllPujaservices() {
        Utilz.showProgress(Profile.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(Profile.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(Profile.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.pujaListForPurohit(sessiontoken,auth, new DownlodableCallback<PujaListModel>() {
            @Override
            public void onSuccess(final PujaListModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                        pujalistModels = result.getResult();
                        Intent intent=new Intent(Profile.this,PujaListActivity.class);
                        intent.putParcelableArrayListExtra("list", pujalistModels);
                        intent.putStringArrayListExtra("Id", pujaid);
                        intent.putExtra("edit", "puja");
                        startActivityForResult(intent, 5);
                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(Profile.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(Profile.this, Login.class));
                finish();
            }
        });
    }

    private void getAllLanguage() {
        Utilz.showProgress(Profile.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(Profile.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(Profile.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.languageList(sessiontoken,auth, new DownlodableCallback<LanguageListModel>() {
            @Override
            public void onSuccess(final LanguageListModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    langlistModels = result.getResult();
                        Intent intent=new Intent(Profile.this,LanguageListActivity.class);
                        intent.putParcelableArrayListExtra("list", langlistModels);
                        intent.putStringArrayListExtra("Id", langid);
                        intent.putExtra("edit", "lan");
                        startActivityForResult(intent, 2);

                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(Profile.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(Profile.this, Login.class));
                finish();
            }
        });

    }

    private void getPurohitDetails() {
        Utilz.showProgress(Profile.this, getResources().getString(R.string.pleasewait));
            String sessiontoken = ClsGeneral.getPreferences(Profile.this, PreferenceName.USER_SESSION_TOKEN);
            String auth = "Bearer "+ClsGeneral.getPreferences(Profile.this,PreferenceName.USER_TOKEN);
            retrofitDataProvider.purohitDetails(sessiontoken, auth, new DownlodableCallback<PurohitDetailsModel>() {
                @Override
                public void onSuccess(final PurohitDetailsModel result) {
                    Utilz.dismissProgress();
                    if (result.getApiStatus().contains(PreferenceName.TRUE)) {

                        setPurohitDetails(result.getResult().getUser());
                        setPurohitLanguage(result.getResult().getLanguageDetails());
                        setPurohitPujaServices(result.getResult().getPujaDetails());
                        setLocationToMap(result.getResult().getLatitude(), result.getResult().getLongitude(), result.getResult().getAddress());
                        ClsGeneral.setPreferences(Profile.this, PreferenceName.SEEKRANGE, result.getResult().getArea_of_service());

                    }
                    else
                    {

                        Utilz.dismissProgress();
                    }


                }

                @Override
                public void onFailure(String error) {
                    Utilz.dismissProgress();
                }

                @Override
                public void onUnauthorized(int errorNumber) {

                    ClsGeneral.setPreferences(Profile.this, PreferenceName.USER_ID, "");
                    startActivity(new Intent(Profile.this, Login.class));
                    finish();
                }
            });
        }

    private void setPurohitPujaServices(ArrayList<PurohitSelectedPujaModel> pujaDetails) {
        String pujaservice = "";

        for (int i=0; i<pujaDetails.size(); i++){
            pujaid.add(pujaDetails.get(i).getId());
            if (pujaservice.equals(""))
            {
                pujaservice= pujaDetails.get(i).getPuja_name();
            }
            else {
                pujaservice = pujaservice+", "+pujaDetails.get(i).getPuja_name();
            }
        }
        purohitPujaServices.setText(pujaservice);
    }

    private void setLocationToMap(String latitude, String longitude, String address) {
        ClsGeneral.setPreferences(Profile.this, PreferenceName.LATITUTE,latitude);
        ClsGeneral.setPreferences(Profile.this, PreferenceName.LONGITUTE,longitude);
        Double lat = null,lon = null;
        try {
            lat = Double.parseDouble(latitude);
            lon = Double.parseDouble(longitude);
        }
        catch (NumberFormatException e){

            lat = 13.027231;
            lon = 77.577572;
        }
        catch (Exception e){
            lat = 13.027231;
            lon = 77.577572;
        }
        showMap(lat, lon, address);

    }

    private void setPurohitLanguage(ArrayList<PurohitSelectedLangModel> languageDetails) {

        String lang = "";

        for (int i=0;i<languageDetails.size(); i++) {
            langid.add(languageDetails.get(i).getId());
            if (lang.equals("")){
                lang = languageDetails.get(i).getLanguage_name();
            }
            else
            {
                lang = lang+", "+languageDetails.get(i).getLanguage_name();
            }
        }

        purohitLanguage.setText(lang);
    }

    private void setPurohitDetails(PurohitUserModel user) {

        purohitName.setText(user.getFirst_name());
        userMobile.setText(user.getMobile_number());
        purohitEmail.setText(user.getEmail());
    }

    private void initWidgit() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getView().setClickable(false);
        mapFragment.getMapAsync(this);

        button_submit.setText("Logout");
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
        googleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Your Location"));
        drawCircle();

//        googleMap.setMyLocationEnabled(true);


    }

    @OnClick(R.id.tv_locationEdit)
    void onEditClick(){
        startActivity(new Intent(Profile.this, SelectLocatonNservices.class));
    }

    private void drawCircle() {
        if(circle!=null){
            circle.remove();
        }
        circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latLng.latitude, latLng.longitude))
                .radius(1300)
                .strokeWidth(5)
                .strokeColor(Color.RED)
                .fillColor(Color.TRANSPARENT));
    }
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    protected void onResume() {
        super.onResume();

        Double lat = null,lon = null;
        try {
            lat = Double.parseDouble(ClsGeneral.getPreferences(Profile.this, PreferenceName.LATITUTE));
            lon = Double.parseDouble(ClsGeneral.getPreferences(Profile.this, PreferenceName.LONGITUTE));
        }
        catch (NumberFormatException e){

            lat = 13.027231;
            lon = 77.577572;
        }
        catch (Exception e){
            lat = 13.027231;
            lon = 77.577572;
        }
        showMap(lat, lon, "Bangalore");
    }

    private void showMap(Double lat, Double lon, String address) {

        latLng = new LatLng(lat,lon);
        try{
            if (mMap!=null){
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title(address));
            }

        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            try {
                String message = data.getStringExtra("MESSAGE");
                langid = data.getStringArrayListExtra("ID");
                purohitLanguage.setText(message);
                getPurohitDetails();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
        else if(requestCode==5)
        {
            try {
                String message = data.getStringExtra("MESSAGE");
                pujaid = data.getStringArrayListExtra("ID");
                purohitPujaServices.setText(message);
                getPurohitDetails();
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }

    }
}
