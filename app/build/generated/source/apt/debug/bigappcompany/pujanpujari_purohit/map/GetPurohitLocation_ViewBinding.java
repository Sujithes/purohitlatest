// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.map;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class GetPurohitLocation_ViewBinding implements Unbinder {
  private GetPurohitLocation target;

  @UiThread
  public GetPurohitLocation_ViewBinding(GetPurohitLocation target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public GetPurohitLocation_ViewBinding(GetPurohitLocation target, View source) {
    this.target = target;

    target.iv_currentLocation = Utils.findRequiredViewAsType(source, R.id.iv_currentLocation, "field 'iv_currentLocation'", ImageView.class);
    target.myToolbar = Utils.findRequiredViewAsType(source, R.id.my_toolbar, "field 'myToolbar'", Toolbar.class);
    target.sumbitButton = Utils.findRequiredViewAsType(source, R.id.button_submit, "field 'sumbitButton'", Button.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    GetPurohitLocation target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_currentLocation = null;
    target.myToolbar = null;
    target.sumbitButton = null;
  }
}
