// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Login_ViewBinding implements Unbinder {
  private Login target;

  private View view2131296323;

  @UiThread
  public Login_ViewBinding(Login target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Login_ViewBinding(final Login target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'onButtonClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick();
      }
    });
    target.et_mobile = Utils.findRequiredViewAsType(source, R.id.et_mobile, "field 'et_mobile'", EditText.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    Login target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.button_submit = null;
    target.et_mobile = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
  }
}
