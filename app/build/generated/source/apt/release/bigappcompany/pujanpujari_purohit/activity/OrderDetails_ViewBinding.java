// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OrderDetails_ViewBinding implements Unbinder {
  private OrderDetails target;

  private View view2131296688;

  private View view2131296668;

  private View view2131296302;

  private View view2131296677;

  @UiThread
  public OrderDetails_ViewBinding(OrderDetails target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public OrderDetails_ViewBinding(final OrderDetails target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.tv_reject, "field 'tv_reject' and method 'OnTextClick'");
    target.tv_reject = Utils.castView(view, R.id.tv_reject, "field 'tv_reject'", TextView.class);
    view2131296688 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnTextClick(Utils.castParam(p0, "doClick", 0, "OnTextClick", 0, TextView.class));
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_accept, "field 'tv_accept' and method 'OnTextClick'");
    target.tv_accept = Utils.castView(view, R.id.tv_accept, "field 'tv_accept'", TextView.class);
    view2131296668 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.OnTextClick(Utils.castParam(p0, "doClick", 0, "OnTextClick", 0, TextView.class));
      }
    });
    target.iv_userimage = Utils.findRequiredViewAsType(source, R.id.iv_userimage, "field 'iv_userimage'", CircularImageView.class);
    target.tv_userName = Utils.findRequiredViewAsType(source, R.id.tv_userName, "field 'tv_userName'", TextView.class);
    target.tv_mobile = Utils.findRequiredViewAsType(source, R.id.tv_mobile, "field 'tv_mobile'", TextView.class);
    target.iv_product = Utils.findRequiredViewAsType(source, R.id.iv_product, "field 'iv_product'", CircularImageView.class);
    target.tv_productname = Utils.findRequiredViewAsType(source, R.id.tv_productname, "field 'tv_productname'", TextView.class);
    target.tv_date = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tv_date'", TextView.class);
    target.tv_time = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tv_time'", TextView.class);
    target.tv_language = Utils.findRequiredViewAsType(source, R.id.tv_language, "field 'tv_language'", TextView.class);
    target.tv_address = Utils.findRequiredViewAsType(source, R.id.tv_address, "field 'tv_address'", TextView.class);
    view = Utils.findRequiredView(source, R.id.back, "field 'back' and method 'onBackImage'");
    target.back = Utils.castView(view, R.id.back, "field 'back'", ImageView.class);
    view2131296302 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBackImage();
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_delivryaddress, "field 'tv_delivryaddress' and method 'mapClick'");
    target.tv_delivryaddress = Utils.castView(view, R.id.tv_delivryaddress, "field 'tv_delivryaddress'", TextView.class);
    view2131296677 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.mapClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    OrderDetails target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tv_reject = null;
    target.tv_accept = null;
    target.iv_userimage = null;
    target.tv_userName = null;
    target.tv_mobile = null;
    target.iv_product = null;
    target.tv_productname = null;
    target.tv_date = null;
    target.tv_time = null;
    target.tv_language = null;
    target.tv_address = null;
    target.back = null;
    target.tv_delivryaddress = null;

    view2131296688.setOnClickListener(null);
    view2131296688 = null;
    view2131296668.setOnClickListener(null);
    view2131296668 = null;
    view2131296302.setOnClickListener(null);
    view2131296302 = null;
    view2131296677.setOnClickListener(null);
    view2131296677 = null;
  }
}
