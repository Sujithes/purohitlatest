package bigappcompany.pujanpujari_purohit.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.map.SearchAddressGooglePlacesActivity;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 20/2/18.
 */

public class SelectLocatonNservices extends BaseActivity implements OnMapReadyCallback{
    private static final String TAG = "MapActivity";
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.tv_locationEdit)
    TextView tv_locationEdit;
    @BindView(R.id.tv_locationName)
    TextView tv_locationName;
    @BindView(R.id.tv_seekbarEdit)
    TextView tv_seekbarEdit;
    @BindView(R.id.seekrange)
    TextView seekrange;
    private Circle circle;
    private LatLng latLng;
    private GoogleMap mMap;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activirt_servies_location);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        initWidgit();
    }


    @Override
    protected void onResume() {
        super.onResume();

        Double lat = null,lon = null;
        try {
             lat = Double.parseDouble(ClsGeneral.getPreferences(SelectLocatonNservices.this, PreferenceName.LATITUTE));
             lon = Double.parseDouble(ClsGeneral.getPreferences(SelectLocatonNservices.this, PreferenceName.LONGITUTE));
        }
        catch (NumberFormatException e){

            lat = 13.027231;
            lon = 77.577572;
        }
        catch (Exception e){
            lat = 13.027231;
            lon = 77.577572;
        }
        latLng = new LatLng(lat,lon);
        try {
            if (mMap!=null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title("Your Location"));
            }
        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }

        String seekbarrange = ClsGeneral.getPreferences(SelectLocatonNservices.this, PreferenceName.SEEKRANGE);
        try {
            seekrange.setText(seekbarrange+" km");
        }catch (NumberFormatException e){
            seekrange.setText("2 km");
        }catch (Exception e){

        }
    }

    private void initWidgit() {

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap =googleMap;
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        googleMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
        googleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Your Location"));
        drawCircle(googleMap);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @OnClick({R.id.tv_locationEdit, R.id.tv_seekbarEdit})
    void onEditClick(TextView textView ){

            if(textView.getId() == R.id.tv_locationEdit){
                startActivity(new Intent(SelectLocatonNservices.this, SearchAddressGooglePlacesActivity.class));
            }
            else if(textView.getId() == R.id.tv_seekbarEdit){
                startActivity(new Intent(SelectLocatonNservices.this, ServiceRadius.class).putExtra("from","other"));

            }
    }

    private void drawCircle(GoogleMap googleMap) {
        if(circle!=null){
            circle.remove();
        }
        circle = googleMap.addCircle(new CircleOptions()
                .center(new LatLng(latLng.latitude, latLng.longitude))
                .radius(1300)
                .strokeWidth(5)
                .strokeColor(Color.RED)
                .fillColor(Color.TRANSPARENT));
    }

    @OnClick(R.id.iv_back)
    void onBackClick(){
        finish();
    }
}
