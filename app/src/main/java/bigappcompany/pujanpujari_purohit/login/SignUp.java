package bigappcompany.pujanpujari_purohit.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.activity.LanguageListActivity;
import bigappcompany.pujanpujari_purohit.activity.PujaListActivity;
import bigappcompany.pujanpujari_purohit.activity.ServiceRadius;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.map.GetPurohitLocation;
import bigappcompany.pujanpujari_purohit.model.LanguageListModel;
import bigappcompany.pujanpujari_purohit.model.LanguageResult;
import bigappcompany.pujanpujari_purohit.model.PujaListModel;
import bigappcompany.pujanpujari_purohit.model.PujaListResultModel;
import bigappcompany.pujanpujari_purohit.model.SignUpModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.ProgressGenerator;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 15/2/18.
 */

public class SignUp extends AppCompatActivity implements ProgressGenerator.OnCompleteListener{
    @BindView(R.id.iv_userImage)
    ImageView iv_userImage;
    @BindView(R.id.input_layout_name)
    TextInputLayout input_layout_name;
    @BindView(R.id.input_name)
    EditText input_name;
    @BindView(R.id.input_layout_mobile)
    TextInputLayout input_layout_mobile;
    @BindView(R.id.input_mobile)
    EditText input_mobile;
    @BindView(R.id.input_layout_email)
    TextInputLayout input_layout_email;
    @BindView(R.id.input_email)
    EditText input_email;
    @BindView(R.id.input_location)
    TextView input_location;
    @BindView(R.id.input_language)
    TextView input_language;
    @BindView(R.id.input_areaofservice)
    TextView input_areaofservice;
    @BindView(R.id.input_pujaservice)
    TextView input_pujaservice;
    @BindView(R.id.button_submit)
    Button button_submit;
    @BindView(R.id.rr_location)
    RelativeLayout rr_location;

    private RetrofitDataProvider retrofitDataProvider;
    private String lat,lon;
    private ArrayList<LanguageResult> langlistModels = new ArrayList<>();
    private ArrayList<String> langid = new ArrayList<>();
    private int all_id[];

    private ArrayList<PujaListResultModel> pujalistModels = new ArrayList<>();
    private ArrayList<String> pujaid = new ArrayList<>();
    private int all_pujaid[];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);
        initWidgit();
        getAllLanguage(false);

    }



    private void getAllLanguage(final boolean status) {
        String sessiontoken = ClsGeneral.getPreferences(SignUp.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(SignUp.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.languageList(sessiontoken,auth, new DownlodableCallback<LanguageListModel>() {
            @Override
            public void onSuccess(final LanguageListModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    langlistModels = result.getResult();
                    if (status){
                        Intent intent=new Intent(SignUp.this,LanguageListActivity.class);
                        intent.putParcelableArrayListExtra("list", langlistModels);
                        intent.putStringArrayListExtra("Id", langid);
                        startActivityForResult(intent, 2);
                    }

                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(SignUp.this, Login.class));
                finish();
            }
        });

        getAllPujaservices(false);
    }

    private void getAllPujaservices(final boolean status) {
        String sessiontoken = ClsGeneral.getPreferences(SignUp.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(SignUp.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.pujaListForPurohit(sessiontoken,auth, new DownlodableCallback<PujaListModel>() {
            @Override
            public void onSuccess(final PujaListModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    pujalistModels = result.getResult();
                    if (status) {
                        Intent intent = new Intent(SignUp.this, PujaListActivity.class);
                        intent.putParcelableArrayListExtra("list", pujalistModels);
                        intent.putStringArrayListExtra("Id", pujaid);
                        intent.putExtra("edit", "no");
                        startActivityForResult(intent, 5);
                    }

                }
            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(SignUp.this, Login.class));
                finish();
            }
        });
    }

    private void initWidgit() {
        button_submit.setText(getResources().getString(R.string.text_submit));
        input_name.addTextChangedListener(new MyTextWatcher(input_name));
        input_mobile.addTextChangedListener(new MyTextWatcher(input_mobile));
        input_email.addTextChangedListener(new MyTextWatcher(input_email));

        input_language.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateLanguage();
            }
        });

        input_pujaservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validatePujaService();
            }
        });

        input_areaofservice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(SignUp.this,ServiceRadius.class);
                intent.putExtra("from", "signup");
                startActivityForResult(intent, 3);
            }
        });


        try{
            input_mobile.setText(getIntent().getStringExtra("number"));
        }
        catch (Exception e){

        }
    }


    @OnClick(R.id.button_submit)
    void onButtonClick() {

        doSignUp();
    }

    @OnClick(R.id.rr_location)
    void onLocationClick(){
        Intent intent=new Intent(SignUp.this,GetPurohitLocation.class);
        startActivityForResult(intent, 4);
    }

    private void doSignUp() {
        validateField();
    }

    private void validateField() {

        if (!validateName()) {
            return;
        }
        if (!validatemobile()) {
            return;
        }

        if (!validateEmail()) {
            return;
        }
        if (!validateLocation()) {
            return;
        }

        else {
            callSignUpApi();
           // startActivity(new Intent(SignUp.this, MainActivity.class));
        }



    }

    private void callSignUpApi() {
        Utilz.showProgress(SignUp.this, getResources().getString(R.string.pleasewait));
        retrofitDataProvider.signupPurohit(input_name.getText().toString(), "", PreferenceName.USERROLE, input_email.getText().toString(),
                input_mobile.getText().toString(),  input_location.getText().toString(), lat, lon, input_areaofservice.getText().toString(), new DownlodableCallback<SignUpModel>() {
            @Override
            public void onSuccess(final SignUpModel result) {

                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                  //  ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, result.getUser().getId());
                    ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_MOBILE, result.getUser().getMobile_number());

                    try
                    {
                        ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_TOKEN, result.getToken());
                        ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_SESSION_TOKEN, result.getUser().getSession_token());
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                    ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_EMAIL, result.getUser().getEmail());
                    ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_NAME, result.getUser().getFirst_name());
                    ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ADDRESS, result.getUser().getAddress());

                    callSaveLanguageApi();

                }else {
                    Utilz.amazingMessage(SignUp.this, result.getResult(),  PreferenceName.ERROR_MESSAGE);
                    Utilz.dismissProgress();
                }



            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(SignUp.this, Login.class));
                finish();
            }
        });
    }

    private void callSaveLanguageApi() {
        all_id = new int[langid.size()];
        for (int i=0; i<langid.size();i++){
            all_id[i] = Integer.valueOf(langid.get(i));
        }
        Utilz.showProgress(SignUp.this, getResources().getString(R.string.save_language));
        String sessiontoken = ClsGeneral.getPreferences(SignUp.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(SignUp.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.saveLanguage(sessiontoken, auth, all_id, new DownlodableCallback<StatusResultModel>() {
                    @Override
                    public void onSuccess(final StatusResultModel result) {
                        Utilz.dismissProgress();
                        callSavePujaServiceApi();
                        if (result.getApiStatus().contains(PreferenceName.TRUE)) {

                        }
                        else
                        {
                        }
                    }

                    @Override
                    public void onFailure(String error) {
                        Utilz.dismissProgress();
                        callSavePujaServiceApi();
                    }

                    @Override
                    public void onUnauthorized(int errorNumber) {

                        ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, "");
                        startActivity(new Intent(SignUp.this, Login.class));
                        finish();
                    }
                });


    }

    private void callSavePujaServiceApi() {
        all_pujaid = new int[pujaid.size()];
        for (int i=0; i<langid.size();i++){
            all_pujaid[i] = Integer.valueOf(pujaid.get(i));
        }
        Utilz.showProgress(SignUp.this, getResources().getString(R.string.save_language));
        String sessiontoken = ClsGeneral.getPreferences(SignUp.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(SignUp.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.savePujaService(sessiontoken, auth, all_pujaid, new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID,"");
                    startActivity(new Intent(SignUp.this, Login.class));
                    finish();
                }
                else
                {

                    Utilz.dismissProgress();
                }


            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(SignUp.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(SignUp.this, Login.class));
                finish();
            }
        });
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onComplete() {

    }

    public class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.input_name:
                    validateName();
                    break;
                case R.id.input_mobile:
                    validatemobile();
                    break;
                case R.id.input_email:
                    validateEmail();
                    break;
                case R.id.input_location:
                    validateLocation();
                    break;


            }
        }
    }



    private boolean validatePujaService() {
        /*if (langlistModels.size()==0){
            getAllPujaservices(true);
        }else {
            Intent intent=new Intent(SignUp.this,PujaListActivity.class);
            intent.putParcelableArrayListExtra("list", pujalistModels);
            intent.putStringArrayListExtra("Id", pujaid);
            startActivityForResult(intent, 5);
        }
        return true;*/
        if (pujalistModels.size()==0){
            getAllPujaservices(true);
        }else {
            Intent intent = new Intent(SignUp.this, PujaListActivity.class);
            intent.putParcelableArrayListExtra("list", pujalistModels);
            intent.putStringArrayListExtra("Id", pujaid);
            intent.putExtra("edit", "no");
            startActivityForResult(intent, 5);
        }
        return true;
    }

     private boolean validateLanguage() {
        if (langlistModels.size()==0){
            getAllLanguage(true);
        }else {
            Intent intent=new Intent(SignUp.this,LanguageListActivity.class);
            intent.putParcelableArrayListExtra("list", langlistModels);
            intent.putStringArrayListExtra("Id", langid);
            intent.putExtra("edit", "no");
            startActivityForResult(intent, 2);
        }
        return true;
    }

    private boolean validateLocation() {
        if (input_location.getText().toString().trim().equals("")){
            Utilz.amazingMessage(SignUp.this, getResources().getString(R.string.hint_location), PreferenceName.ERROR_MESSAGE);
            requestFocus(input_location);
            return false;
        }else {
        }
        return true;
    }

    private boolean validatemobile() {
        if (input_mobile.getText().toString().trim().equals("")) {
            input_layout_mobile.setError("Enter mobile number");
            requestFocus(input_mobile);
            return false;
        } else {
            input_layout_mobile.setErrorEnabled(false);
        }
        if (input_mobile.getText().toString().length() < 10) {
            input_layout_mobile.setError("Enter correct mobile number");
            requestFocus(input_mobile);
            return false;
        } else {
            input_layout_mobile.setErrorEnabled(false);
        }

        return true;
    }

    private boolean validateEmail() {
        if (input_email.getText().toString().trim().equals("")) {
            input_layout_email.setError("Enter your email id");
            requestFocus(input_email);
            return false;
        } else {
            input_layout_email.setErrorEnabled(false);
        }
        if (!Utilz.isValidEmail1(input_email.getText().toString())) {
            input_layout_email.setError("Enter valid email id");
            requestFocus(input_email);
            return false;
        } else {
            input_layout_email.setErrorEnabled(false);
        }
        return true;
    }

    private boolean validateName() {
        if (input_name.getText().toString().trim().equals("")) {
            input_layout_name.setError("Enter your name");
            requestFocus(input_name);
            return false;
        } else {
            input_layout_name.setErrorEnabled(false);
        }
        return true;
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==2)
        {
            try {
                String message = data.getStringExtra("MESSAGE");
                langid = data.getStringArrayListExtra("ID");
                input_language.setText(message);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
       else if(requestCode==3)
        {
            try {
                String message = data.getStringExtra("MESSAGE");
                input_areaofservice.setText(message+" km");
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

        }
        else if(requestCode==4)
        {
            String message=data.getStringExtra("MESSAGE");
             lat = data.getStringExtra("lat");
             lon = data.getStringExtra("lon");
            input_location.setText(message);
        }
       else if(requestCode==5)
        {
            try {
                String message = data.getStringExtra("MESSAGE");
                pujaid = data.getStringArrayListExtra("ID");
                input_pujaservice.setText(message);
            }
            catch (Exception e){
                e.printStackTrace();
            }
        }
    }


}