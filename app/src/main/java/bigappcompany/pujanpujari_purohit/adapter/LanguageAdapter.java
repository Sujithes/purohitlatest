package bigappcompany.pujanpujari_purohit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.model.LanguageResult;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shankar on 7/3/18.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    private List<LanguageResult> languageListModels;
    private Context context;
    private int categorylist_row;
    private OnClickListener onClickListener ;
    ArrayList<String> id;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.languageCheck)
        CheckBox languageCheck;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }


    public LanguageAdapter(Context context, ArrayList<LanguageResult> languageListModels, ArrayList<String> id, int categorylist_row, OnClickListener onClickListener) {
        this.context = context;
        this.languageListModels = languageListModels;
        this.categorylist_row = categorylist_row;
        this.onClickListener = onClickListener;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(categorylist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final int pos = position;
        final LanguageResult pl = languageListModels.get(position);

        holder.languageCheck.setText(pl.getLanguage_name());
        if (id.size()>0){
            for (int i=0; i<id.size();i++){
                if (pl.getId().equals(id.get(i))) {
                    holder.languageCheck.setChecked(true);
                }
            }
        }

        holder.languageCheck.setTag(pl);

        /*holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(position);
            }
        });
*/
        holder.languageCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                LanguageResult contact = (LanguageResult) cb.getTag();

                contact.setSelected(cb.isChecked());
                languageListModels.get(pos).setSelected(cb.isChecked());

                /*Toast.makeText(
                        v.getContext(),
                        "Clicked on Checkbox: " + cb.getText() + " is "
                                + cb.isChecked(), Toast.LENGTH_LONG).show();*/
                if (cb.isChecked()){
                    onClickListener.twoParameter(languageListModels.get(pos).getLanguage_name(),"add", pos);
                }
                else {
                    onClickListener.twoParameter(languageListModels.get(pos).getLanguage_name(),"remove", pos);
                }

            }
        });

    }

    @Override
    public int getItemCount() {
        return languageListModels.size();
    }
}

