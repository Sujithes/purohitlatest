// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class MainActivity_ViewBinding implements Unbinder {
  private MainActivity target;

  private View view2131296477;

  private View view2131296479;

  private View view2131296480;

  @UiThread
  public MainActivity_ViewBinding(MainActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public MainActivity_ViewBinding(final MainActivity target, View source) {
    this.target = target;

    View view;
    target.tabLayout = Utils.findRequiredViewAsType(source, R.id.tabLayout, "field 'tabLayout'", TabLayout.class);
    target.pager = Utils.findRequiredViewAsType(source, R.id.pager, "field 'pager'", ViewPager.class);
    target.iv_booking = Utils.findRequiredViewAsType(source, R.id.iv_booking, "field 'iv_booking'", ImageView.class);
    target.tv_booking = Utils.findRequiredViewAsType(source, R.id.tv_booking, "field 'tv_booking'", TextView.class);
    target.iv_callendar = Utils.findRequiredViewAsType(source, R.id.iv_callendar, "field 'iv_callendar'", ImageView.class);
    target.tv_callendar = Utils.findRequiredViewAsType(source, R.id.tv_callendar, "field 'tv_callendar'", TextView.class);
    target.iv_profile = Utils.findRequiredViewAsType(source, R.id.iv_profile, "field 'iv_profile'", ImageView.class);
    target.tv_profile = Utils.findRequiredViewAsType(source, R.id.tv_profile, "field 'tv_profile'", TextView.class);
    view = Utils.findRequiredView(source, R.id.ll_booking, "field 'll_booking' and method 'onLinearLayoutClick'");
    target.ll_booking = Utils.castView(view, R.id.ll_booking, "field 'll_booking'", LinearLayout.class);
    view2131296477 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLinearLayoutClick(Utils.castParam(p0, "doClick", 0, "onLinearLayoutClick", 0, LinearLayout.class));
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_callenda, "field 'll_callenda' and method 'onLinearLayoutClick'");
    target.ll_callenda = Utils.castView(view, R.id.ll_callenda, "field 'll_callenda'", LinearLayout.class);
    view2131296479 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLinearLayoutClick(Utils.castParam(p0, "doClick", 0, "onLinearLayoutClick", 0, LinearLayout.class));
      }
    });
    view = Utils.findRequiredView(source, R.id.ll_profile, "field 'll_profile' and method 'onLinearLayoutClick'");
    target.ll_profile = Utils.castView(view, R.id.ll_profile, "field 'll_profile'", LinearLayout.class);
    view2131296480 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLinearLayoutClick(Utils.castParam(p0, "doClick", 0, "onLinearLayoutClick", 0, LinearLayout.class));
      }
    });
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.mToolbar, "field 'mToolbar'", Toolbar.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    MainActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tabLayout = null;
    target.pager = null;
    target.iv_booking = null;
    target.tv_booking = null;
    target.iv_callendar = null;
    target.tv_callendar = null;
    target.iv_profile = null;
    target.tv_profile = null;
    target.ll_booking = null;
    target.ll_callenda = null;
    target.ll_profile = null;
    target.mToolbar = null;

    view2131296477.setOnClickListener(null);
    view2131296477 = null;
    view2131296479.setOnClickListener(null);
    view2131296479 = null;
    view2131296480.setOnClickListener(null);
    view2131296480 = null;
  }
}
