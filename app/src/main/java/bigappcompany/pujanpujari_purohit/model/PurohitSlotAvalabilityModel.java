package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 12/3/18.
 */

public class PurohitSlotAvalabilityModel {


    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public TimeSlotAvailableModel getResult() {
        return result;
    }

    public void setResult(TimeSlotAvailableModel result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    TimeSlotAvailableModel result;
}
