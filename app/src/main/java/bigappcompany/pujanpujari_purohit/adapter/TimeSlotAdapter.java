package bigappcompany.pujanpujari_purohit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.model.AvailabilityDetails;
import bigappcompany.pujanpujari_purohit.model.TimeSlotResultModel;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shankar on 12/3/18.
 */

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.MyViewHolder> {

    private List<TimeSlotResultModel> timeSlotResultModels;
    private Context context;
    private int categorylist_row;
    private OnClickListener onClickListener ;
    private ArrayList<AvailabilityDetails> availableList;
    private Boolean idwholechecked = false;

    public void setchecked(boolean idwholechecked) {
        this.idwholechecked = idwholechecked;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_timeslot)
        TextView tv_timeslot;
        @BindView(R.id.ll_timeslot)
        LinearLayout ll_timeslot;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }



    public TimeSlotAdapter(Context context, ArrayList<TimeSlotResultModel> timeSlotResultModels, ArrayList<AvailabilityDetails> availableList, int categorylist_row, OnClickListener onClickListener) {
        this.context = context;
        this.timeSlotResultModels = timeSlotResultModels;
        this.availableList = availableList;
        this.categorylist_row = categorylist_row;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(categorylist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final int pos = position;
        final TimeSlotResultModel pl = timeSlotResultModels.get(position);

        if (idwholechecked){
            if (availableList.get(position).isSelected())
            {
                holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorWhite));
                holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundcorner_busy));
                if (availableList.get(position).getAvailable_type().equals("0")){
                    onClickListener.twoParameter(timeSlotResultModels.get(position).getId(), "add", position);
                }
            }
            else {
                holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorSecondary));
                holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundedcorner_available));
                if (availableList.get(position).getAvailable_type().equals("0")){
                    onClickListener.twoParameter(timeSlotResultModels.get(position).getId(), "remove", position);
                }

            }
        }
        holder.tv_timeslot.setText(Utilz.getFromdate(pl.getFrom_time()) +" to "+Utilz.getToTime(pl.getFrom_time()));
        if (availableList.size()>0){
            for (int j=0; j<availableList.size(); j++) {
                if (pl.getId().equals(availableList.get(j).getSlot_id())) {
                    if (availableList.get(j).getAvailable_type().equals("1")) {
                        holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorWhite));
                        holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundcorner_busy));
                        holder.itemView.setClickable(false);
                        holder.itemView.setClickable(false);
                        holder.itemView.setLongClickable(false);
                        holder.itemView.setFocusable(false);
                    } else if (availableList.get(j).getAvailable_type().equals("2")) {
                        holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorWhite));
                        holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundcorner_booked));
                        holder.itemView.setClickable(false);
                        holder.itemView.setLongClickable(false);
                        holder.itemView.setFocusable(false);

                    } else if (availableList.get(j).getAvailable_type().equals("3")) {

                    } else if (availableList.get(j).getAvailable_type().equals("0")) {
                        if (availableList.get(position).isSelected())
                        {
                            holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorWhite));
                            holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundcorner_busy));
                        }
                        else {
                            holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorSecondary));
                            holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundedcorner_available));


                        }
                    }
                }
            }
        }

        holder.ll_timeslot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (availableList.get(position).getAvailable_type().equals("0")){


                if (availableList.get(position).isSelected()){
                    availableList.get(position).setSelected(false);
                    holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorSecondary));
                    holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundedcorner_available));
                    onClickListener.twoParameter(timeSlotResultModels.get(position).getId(),"remove", pos);

                }
                else {
                    availableList.get(position).setSelected(true);
                    holder.tv_timeslot.setTextColor(context.getResources().getColor(R.color.textColorWhite));
                    holder.ll_timeslot.setBackground(context.getResources().getDrawable(R.drawable.roundcorner_busy));
                    onClickListener.twoParameter(timeSlotResultModels.get(position).getId(),"add", pos);
                }


            }
            }
        });




    }

    @Override
    public int getItemCount() {
        return timeSlotResultModels.size();
    }
}


