package bigappcompany.pujanpujari_purohit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 19/2/18.
 */

public class LanguageResult implements Parcelable{
    @SerializedName("language_name")
    String language_name;
    @SerializedName("id")
    String id;

    protected LanguageResult(Parcel in) {
        language_name = in.readString();
        id = in.readString();
        selected = in.readByte() != 0;
    }

    public static final Creator<LanguageResult> CREATOR = new Creator<LanguageResult>() {
        @Override
        public LanguageResult createFromParcel(Parcel in) {
            return new LanguageResult(in);
        }

        @Override
        public LanguageResult[] newArray(int size) {
            return new LanguageResult[size];
        }
    };

    public String getLanguage_name() {
        return language_name;
    }

    public void setLanguage_name(String language_name) {
        this.language_name = language_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    boolean selected;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(language_name);
        dest.writeString(id);
        dest.writeByte((byte) (selected ? 1 : 0));
    }
}
