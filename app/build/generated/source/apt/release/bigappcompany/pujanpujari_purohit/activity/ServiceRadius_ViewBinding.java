// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class ServiceRadius_ViewBinding implements Unbinder {
  private ServiceRadius target;

  private View view2131296323;

  private View view2131296456;

  @UiThread
  public ServiceRadius_ViewBinding(ServiceRadius target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public ServiceRadius_ViewBinding(final ServiceRadius target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'obSubmitClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.obSubmitClick();
      }
    });
    target.seekBar1 = Utils.findRequiredViewAsType(source, R.id.seekBar1, "field 'seekBar1'", SeekBar.class);
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'iv_back' and method 'onBackClick'");
    target.iv_back = Utils.castView(view, R.id.iv_back, "field 'iv_back'", ImageView.class);
    view2131296456 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBackClick();
      }
    });
    target.seekrange = Utils.findRequiredViewAsType(source, R.id.seekrange, "field 'seekrange'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    ServiceRadius target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.button_submit = null;
    target.seekBar1 = null;
    target.iv_back = null;
    target.seekrange = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
    view2131296456.setOnClickListener(null);
    view2131296456 = null;
  }
}
