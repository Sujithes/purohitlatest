package bigappcompany.pujanpujari_purohit.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.mikhaellopez.circularimageview.CircularImageView;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 16/2/18.
 */

public class OrderDetails extends AppCompatActivity {
    @BindView(R.id.tv_reject)
    TextView tv_reject;
    @BindView(R.id.tv_accept)
    TextView tv_accept;
    @BindView(R.id.iv_userimage)
    CircularImageView iv_userimage;
    @BindView(R.id.tv_userName)
    TextView tv_userName;
    @BindView(R.id.tv_mobile)
    TextView tv_mobile;
    @BindView(R.id.iv_product)
    CircularImageView iv_product;
    @BindView(R.id.tv_productname)
    TextView tv_productname;
    @BindView(R.id.tv_date)
    TextView tv_date;
    @BindView(R.id.tv_time)
    TextView tv_time;
    @BindView(R.id.tv_language)
    TextView tv_language;
    @BindView(R.id.tv_address)
    TextView tv_address;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.tv_delivryaddress)
    TextView tv_delivryaddress;

    private RetrofitDataProvider retrofitDataProvider;
    private Double lat,lon;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLocation;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderdetails);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        retrofitDataProvider = new RetrofitDataProvider(this);
        initWidgit();
        setDetails();
    }

    @OnClick(R.id.tv_delivryaddress)
    void mapClick(){
        String url = "http://maps.google.com/maps?saddr="+mLocation.getLatitude()+","+mLocation.getLongitude()+"&daddr="+lat+","+ lon;
//        String url = "https://www.google.com/maps/dir/?api=1&destination=" + lat + "," + lon + "&travelmode=driving";
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        startActivity(intent);
    }

    private void setDetails() {
        try {
            String ln = getIntent().getStringExtra("lname");
            if (ln.equals("null") || ln.equals(null)) {
                tv_userName.setText(getIntent().getStringExtra("fname"));
            } else {
                tv_userName.setText(getIntent().getStringExtra("fname") + " " + getIntent().getStringExtra("lname"));
            }
        }
        catch (Exception e){
            tv_userName.setText(getIntent().getStringExtra("fname"));
        }

        tv_mobile.setText(getIntent().getStringExtra("mobile"));
        tv_productname.setText(getIntent().getStringExtra("pujaname"));
        tv_date.setText(Utilz.dateFormat(getIntent().getStringExtra("date"), 10));
        tv_language.setText(getIntent().getStringExtra("language"));
        tv_address.setText(getIntent().getStringExtra("address1")+""+getIntent().getStringExtra("address2"));
        tv_time.setText(Utilz.getFromdate(getIntent().getStringExtra("fromtime")) +" "+Utilz.getToTime(getIntent().getStringExtra("totime")));


    }

    @OnClick({R.id.tv_reject, R.id.tv_accept})
    void OnTextClick(TextView textView )
    {
        if (textView.getId() == R.id.tv_reject){

            openCustomDialog();

        }
        if (textView.getId() == R.id.tv_accept){


            if (getIntent().getStringExtra("from").equals("new")){
                callAcceptApi();
            }
            else if (getIntent().getStringExtra("from").equals("booked")){
                callUpdateApi("2");
            }
            else if (getIntent().getStringExtra("from").equals("myway")){
                openDialogForCasCompletion(getIntent().getStringExtra("pendingamount"));

            }


        }
    }

    private void openTimeOutDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
        LayoutInflater inflater = OrderDetails.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.waitforpurohitresponse_dialog, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        TextView dummyText = (TextView)dialogView.findViewById(R.id.dummyText);
        Button button_cancel = (Button)dialogView.findViewById(R.id.button_cancel);
        Button button_yes = (Button)dialogView.findViewById(R.id.button_yes);
        button_yes.setText(getResources().getString(R.string.text_ok));
        dummyText.setText(getResources().getString(R.string.text_requestnotaccepted));
        button_cancel.setVisibility(View.GONE);
        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.cancel();
                //callRejectApi();
                startActivity(new Intent(OrderDetails.this, MainActivity.class)
                        .putExtra("pos",0));
                finishAffinity();
            }
        });

        b.show();
    }

    private void openCustomDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(OrderDetails.this);
        LayoutInflater inflater = OrderDetails.this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.waitforpurohitresponse_dialog, null);
        dialogBuilder.setView(dialogView);
        final AlertDialog b = dialogBuilder.create();
        b.setCancelable(false);
        TextView dummyText = (TextView)dialogView.findViewById(R.id.dummyText);
        Button button_cancel = (Button)dialogView.findViewById(R.id.button_cancel);
        Button button_yes = (Button)dialogView.findViewById(R.id.button_yes);
        button_cancel.setText(getResources().getString(R.string.text_cancel));
        button_yes.setText(getResources().getString(R.string.text_yes));
        dummyText.setText(getResources().getString(R.string.text_requestreject));
        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.cancel();
                callRejectApi();
            }
        });
        button_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                b.cancel();
            }
        });

        b.show();

    }

    private void openDialogForCasCompletion(String pendingamount) {
        final Dialog dialog = new Dialog(OrderDetails.this);
        dialog.setContentView(R.layout.custom_dialog);
        Button button_no = (Button) dialog.findViewById(R.id.button_no);
        Button button_yes = (Button) dialog.findViewById(R.id.button_yes);
        TextView tv_message = (TextView)dialog.findViewById(R.id.tv_message);
        tv_message.setText("Have you collected"+ pendingamount+" From customer?");
        // if button is clicked, close the custom dialog
        button_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        button_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                callUpdateApi("3");
            }
        });
        dialog.show();
    }

    private void callUpdateApi(final String s) {
        Utilz.showProgress(OrderDetails.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(OrderDetails.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(OrderDetails.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.updateBooking(sessiontoken,auth, getIntent().getStringExtra("id"), s, new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    if (s.equals("2")) {
                        startActivity(new Intent(OrderDetails.this, MainActivity.class)
                                .putExtra("pos", 3));
                        finishAffinity();
                    }
                    else
                    {
                        startActivity(new Intent(OrderDetails.this, MainActivity.class)
                            .putExtra("pos", 4));
                        finishAffinity();
                    }

                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(OrderDetails.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(OrderDetails.this, Login.class));
                finish();
            }
        });
    }

    @OnClick(R.id.back)
    void onBackImage(){
        finish();
    }

    private void callAcceptApi() {
        Utilz.showProgress(OrderDetails.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(OrderDetails.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(OrderDetails.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.acceptBooking(sessiontoken,auth, getIntent().getStringExtra("id"),new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    startActivity(new Intent(OrderDetails.this, MainActivity.class)
                    .putExtra("pos",1));
                    finishAffinity();
                }
                else
                {
                    if (result.getResult().equals("expired")){
                        openTimeOutDialog();
                    }
                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(getApplicationContext(), "Please check internetConection,Try again later", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(OrderDetails.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(OrderDetails.this, Login.class));
                finish();
            }
        });
    }

    private void callRejectApi() {
        Utilz.showProgress(OrderDetails.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(OrderDetails.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(OrderDetails.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.rejectBooking(sessiontoken,auth, getIntent().getStringExtra("id"),new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    startActivity(new Intent(OrderDetails.this, MainActivity.class)
                            .putExtra("pos",0));
                    finishAffinity();
                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(OrderDetails.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(OrderDetails.this, Login.class));
                finish();
            }
        });
    }

    private void initWidgit() {
        if (getIntent().getStringExtra("from").equals("new")){

        }
        else if (getIntent().getStringExtra("from").equals("booked")){
            tv_reject.setVisibility(View.GONE);
            tv_accept.setText(getResources().getString(R.string.textonmyway));
        }
        else if (getIntent().getStringExtra("from").equals("myway")){
            tv_reject.setVisibility(View.GONE);
            tv_accept.setText(getResources().getString(R.string.texcomplete));
        }

        lat = Double.parseDouble(getIntent().getStringExtra("latitute"));
        lon = Double.parseDouble(getIntent().getStringExtra("longitute"));

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocation = new Location("");
        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }

    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale) {

        } else {
            startLocationPermissionRequest();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {

            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();
            } else {
            }
        }
    }

    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLocation = task.getResult();
                        } else {
                        }
                    }
                });
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(OrderDetails.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);
    }


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
