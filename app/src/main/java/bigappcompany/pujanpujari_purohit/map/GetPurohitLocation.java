package bigappcompany.pujanpujari_purohit.map;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocompleteFragment;
import com.google.android.gms.location.places.ui.PlaceSelectionListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.activity.BaseActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shankar on 17/2/18.
 */

public class GetPurohitLocation extends BaseActivity implements OnMapReadyCallback, LocationListener,  GoogleMap
        .OnMapClickListener, PlaceSelectionListener, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraChangeListener{

    @BindView(R.id.iv_currentLocation)
    ImageView iv_currentLocation;
    @BindView(R.id.my_toolbar)
    Toolbar myToolbar;
    @BindView(R.id.button_submit)
    Button sumbitButton;
    String address;

    private static final String TAG = "MapActivity";
    private GoogleMap mMap;
    private LatLng mCurrentLocation;
    private PlaceAutocompleteFragment autocompleteFragment;
    private AddressResultReceiver mResultReceiver;
    private FusedLocationProviderClient mFusedLocationClient;
    private Location mLocation;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;
    private String lat,lon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.getpurohitlocation);
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        mLocation = new Location("");
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Roboto-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        initWidgit();
    }

    private void initWidgit() {
        setSupportActionBar(myToolbar);
        myToolbar.setTitle("");
        myToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("MESSAGE", address);
                intent.putExtra("lat", ""+lat);
                intent.putExtra("lon", ""+lon);
                setResult(4,intent);
                finish();
            }
        });

        sumbitButton.setText("SAVE");
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        autocompleteFragment = (PlaceAutocompleteFragment)
                getFragmentManager().findFragmentById(R.id.place_autocomplete_fragment);
        autocompleteFragment.setOnPlaceSelectedListener(this);
        EditText placeET = (EditText) autocompleteFragment.getView().findViewById(R.id.place_autocomplete_search_input);
        placeET.setTextSize(14f);
        ((View)findViewById(R.id.place_autocomplete_search_button)).setVisibility(View.GONE);
        ((View)findViewById(R.id.place_autocomplete_clear_button)).setVisibility(View.GONE);

        mResultReceiver = new AddressResultReceiver(new Handler());

        sumbitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent();
                intent.putExtra("MESSAGE", address);
                intent.putExtra("lat", ""+lat);
                intent.putExtra("lon", ""+lon);
                setResult(4,intent);
                finish();

            }
        });


    }

    private class AddressResultReceiver extends ResultReceiver {
        AddressResultReceiver(Handler handler) {
            super(handler);
        }

        @Override
        protected void onReceiveResult(int resultCode, Bundle resultData) {
            address = resultData.getString(AddressResolverService.LOCATION_DATA_STREET);
            try {
                lat = resultData.getString(AddressResolverService.LAT);
                lon = resultData.getString(AddressResolverService.LON);
                autocompleteFragment.setText(address);
            } catch (NullPointerException e) {
                Log.e(TAG, e.getMessage());
            }
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnCameraChangeListener(this);
        // Setting a click event handler for the map
        googleMap.setOnMapClickListener(this);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        googleMap.setMyLocationEnabled(false    );


        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }

    }


    @Override
    public void onLocationChanged(Location location) {
        onMapClick(new LatLng(location.getLatitude(), location.getLongitude()));
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onMapClick(LatLng latLng) {
        mMap.clear();
        mCurrentLocation = latLng;
        mLocation.setLatitude(latLng.latitude);
        mLocation.setLongitude(latLng.longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        startIntentService(mLocation);
        //drawCircle(latLng);



    }




    @Override
    public void onPlaceSelected(Place place) {
        Log.i(TAG, "Place: " + place.getName());
        onMapClick(place.getLatLng());
    }

    @Override
    public void onError(Status status) {

    }

    @Override
    public void onCameraMove() {

    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        mCurrentLocation = cameraPosition.target;

        mMap.clear();

        try {
            mLocation.setLatitude(mCurrentLocation.latitude);
            mLocation.setLongitude(mCurrentLocation.longitude);
            startIntentService(mLocation);
            //drawCircle(new LatLng(mCurrentLocation.latitude, mCurrentLocation.longitude));
        } catch (Exception e) {
            Log.e(TAG, e.getMessage());
        }
    }

    protected void startIntentService(Location mLocation) {
        Intent intent = new Intent(this, AddressResolverService.class);
        intent.putExtra(AddressResolverService.RECEIVER, mResultReceiver);
        intent.putExtra(AddressResolverService.LOCATION_DATA_EXTRA, mLocation);

        AddressResolverService.enqueueWork(this, intent);
    }


    @Override
    public void onStart() {
        super.onStart();

        if (!checkPermissions()) {
            requestPermissions();
        } else {
            getLastLocation();
        }
    }


    @SuppressWarnings("MissingPermission")
    private void getLastLocation() {
        mFusedLocationClient.getLastLocation()
                .addOnCompleteListener(this, new OnCompleteListener<Location>() {
                    @Override
                    public void onComplete(@NonNull Task<Location> task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            mLocation = task.getResult();
                            mCurrentLocation = new LatLng(mLocation.getLatitude(),mLocation.getLongitude());
                            startIntentService(mLocation);
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mCurrentLocation, 15.0f));
                            //drawCircle(mCurrentLocation);
                        } else {
                            Log.w(TAG, "getLastLocation:exception", task.getException());
                        }
                    }
                });
    }
    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void startLocationPermissionRequest() {
        ActivityCompat.requestPermissions(GetPurohitLocation.this,
                new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                REQUEST_PERMISSIONS_REQUEST_CODE);

    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.");

        } else {
            Log.i(TAG, "Requesting permission");
            startLocationPermissionRequest();
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionResult");
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission granted.
                getLastLocation();

            } else {
            }
        }
    }


    @Override
    public void onBackPressed() {

    }
}

