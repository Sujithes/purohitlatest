// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.design.widget.TextInputLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SignUp_ViewBinding implements Unbinder {
  private SignUp target;

  private View view2131296323;

  private View view2131296571;

  @UiThread
  public SignUp_ViewBinding(SignUp target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SignUp_ViewBinding(final SignUp target, View source) {
    this.target = target;

    View view;
    target.iv_userImage = Utils.findRequiredViewAsType(source, R.id.iv_userImage, "field 'iv_userImage'", ImageView.class);
    target.input_layout_name = Utils.findRequiredViewAsType(source, R.id.input_layout_name, "field 'input_layout_name'", TextInputLayout.class);
    target.input_name = Utils.findRequiredViewAsType(source, R.id.input_name, "field 'input_name'", EditText.class);
    target.input_layout_mobile = Utils.findRequiredViewAsType(source, R.id.input_layout_mobile, "field 'input_layout_mobile'", TextInputLayout.class);
    target.input_mobile = Utils.findRequiredViewAsType(source, R.id.input_mobile, "field 'input_mobile'", EditText.class);
    target.input_layout_email = Utils.findRequiredViewAsType(source, R.id.input_layout_email, "field 'input_layout_email'", TextInputLayout.class);
    target.input_email = Utils.findRequiredViewAsType(source, R.id.input_email, "field 'input_email'", EditText.class);
    target.input_location = Utils.findRequiredViewAsType(source, R.id.input_location, "field 'input_location'", TextView.class);
    target.input_language = Utils.findRequiredViewAsType(source, R.id.input_language, "field 'input_language'", TextView.class);
    target.input_areaofservice = Utils.findRequiredViewAsType(source, R.id.input_areaofservice, "field 'input_areaofservice'", TextView.class);
    target.input_pujaservice = Utils.findRequiredViewAsType(source, R.id.input_pujaservice, "field 'input_pujaservice'", TextView.class);
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'onButtonClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.rr_location, "field 'rr_location' and method 'onLocationClick'");
    target.rr_location = Utils.castView(view, R.id.rr_location, "field 'rr_location'", RelativeLayout.class);
    view2131296571 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onLocationClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    SignUp target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_userImage = null;
    target.input_layout_name = null;
    target.input_name = null;
    target.input_layout_mobile = null;
    target.input_mobile = null;
    target.input_layout_email = null;
    target.input_email = null;
    target.input_location = null;
    target.input_language = null;
    target.input_areaofservice = null;
    target.input_pujaservice = null;
    target.button_submit = null;
    target.rr_location = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
    view2131296571.setOnClickListener(null);
    view2131296571 = null;
  }
}
