package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 9/3/18.
 */

public class PurohitDetailsResultModel {
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getArea_of_service() {
        return area_of_service;
    }

    public void setArea_of_service(String area_of_service) {
        this.area_of_service = area_of_service;
    }

    public ArrayList<PurohitSelectedLangModel> getLanguageDetails() {
        return languageDetails;
    }

    public void setLanguageDetails(ArrayList<PurohitSelectedLangModel> languageDetails) {
        this.languageDetails = languageDetails;
    }

    public ArrayList<PurohitSelectedPujaModel> getPujaDetails() {
        return pujaDetails;
    }

    public void setPujaDetails(ArrayList<PurohitSelectedPujaModel> pujaDetails) {
        this.pujaDetails = pujaDetails;
    }

    public PurohitUserModel getUser() {
        return user;
    }

    public void setUser(PurohitUserModel user) {
        this.user = user;
    }

    @SerializedName("address")
    String address;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;
    @SerializedName("area_of_service")
    String area_of_service;
    @SerializedName("languageDetails")
    ArrayList<PurohitSelectedLangModel> languageDetails;
    @SerializedName("pujaDetails")
    ArrayList<PurohitSelectedPujaModel> pujaDetails;
    @SerializedName("user")
    PurohitUserModel user;
}
