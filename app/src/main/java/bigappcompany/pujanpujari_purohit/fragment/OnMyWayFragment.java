package bigappcompany.pujanpujari_purohit.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.activity.OrderDetails;
import bigappcompany.pujanpujari_purohit.adapter.NewRequestAdapter;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.UserBookingListModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shankar on 21/3/18.
 */

public class OnMyWayFragment extends Fragment {

    @BindView(R.id.common_recyclerview)
    RecyclerView common_recyclerview;

    private RetrofitDataProvider retrofitDataProvider;




    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_new,container, false);
        ButterKnife.bind(this,view);
        retrofitDataProvider = new RetrofitDataProvider(getActivity());
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initWidgit(view);
        getAllNewRequest();

    }

    private void getAllNewRequest() {
        Utilz.showProgress(getActivity(), getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(getActivity(), PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(getActivity(),PreferenceName.USER_TOKEN);
        retrofitDataProvider.userBookedToCompleted(sessiontoken, auth, "2", new DownlodableCallback<UserBookingListModel>() {
            @Override
            public void onSuccess(final UserBookingListModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {

                    common_recyclerview.setAdapter(new NewRequestAdapter(getActivity(), result.getResult(), R.layout.common_order_row, new OnClickListener() {
                        @Override
                        public void onClick(int pos) {
                            startActivity(new Intent(getActivity(), OrderDetails.class)
                                    .putExtra("address1", result.getResult().get(pos).getAddress())
                                    .putExtra("address2", result.getResult().get(pos).getAddress1())
                                    .putExtra("amount", result.getResult().get(pos).getAdvance_amount())
                                    .putExtra("bookingdate", result.getResult().get(pos).getBooking_date())
                                    .putExtra("bookingstatus", result.getResult().get(pos).getBooking_status())
                                    .putExtra("landmark", result.getResult().get(pos).getLand_mark())
                                    .putExtra("language", result.getResult().get(pos).getPurohit_language())
                                    .putExtra("latitute", result.getResult().get(pos).getLatitude())
                                    .putExtra("longitute", result.getResult().get(pos).getLongitude())
                                    .putExtra("paymentstatus", result.getResult().get(pos).getPayment_status())
                                    .putExtra("paymenttype", result.getResult().get(pos).getPayment_type())
                                    .putExtra("id", result.getResult().get(pos).getId())
                                    .putExtra("pujaname", result.getResult().get(pos).getPurohit_puja())
                                    .putExtra("fname", result.getResult().get(pos).getUser_details().getFirst_name())
                                    .putExtra("email", result.getResult().get(pos).getUser_details().getEmail())
                                    .putExtra("lname", result.getResult().get(pos).getUser_details().getLast_name())
                                    .putExtra("mobile", result.getResult().get(pos).getUser_details().getMobile_number())
                                    .putExtra("userprofile", result.getResult().get(pos).getUser_details().getProfile_image())
                                    .putExtra("fromtime", result.getResult().get(pos).getTime_slot().get(0).getSlot_id().getFrom_time())
                                    .putExtra("totime", result.getResult().get(pos).getTime_slot().get(0).getSlot_id().getTo_time())
                                    .putExtra("date", result.getResult().get(pos).getBooking_date())
                                    .putExtra("pendingamount", result.getResult().get(pos).getPending_amount())
                                    .putExtra("from", "myway")
                            );
                        }

                        @Override
                        public void twoParameter(String text1, String text2, int pos) {

                        }
                    }));
                }
                else
                {
                }
                try {
                    Utilz.dismissProgress();
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }

            @Override
            public void onFailure(String error) {
                ClsGeneral.setPreferences(getActivity(), PreferenceName.USER_ID, "");
                startActivity(new Intent(getActivity(), Login.class));
                getActivity().finish();
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

            }
        });
    }

    private void initWidgit(View view) {
        common_recyclerview.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

   /* @OnClick(R.id.topLayout)
    void Onclick(){
        startActivity(new Intent(getActivity(), OrderDetails.class));
    }*/

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}


