package bigappcompany.pujanpujari_purohit.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.activity.MainActivity;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.model.UserDetailsModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.ProgressGenerator;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 8/2/18.
 */

public class Login extends AppCompatActivity implements ProgressGenerator.OnCompleteListener {
    @BindView(R.id.button_submit)
    Button button_submit;
    @BindView(R.id.et_mobile)
    EditText et_mobile;


    private RetrofitDataProvider retrofitDataProvider;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);
        initWidgit();

        if (ClsGeneral.getPreferences(Login.this, PreferenceName.USER_ID).equals("")) {

        } else {
            startActivity(new Intent(Login.this, MainActivity.class)
                    .putExtra("pos", 0));
            finish();
        }


    }


    @OnClick(R.id.button_submit)
    void onButtonClick() {

        if (et_mobile.getText().toString().equals("")) {
            Utilz.amazingMessage(Login.this, getResources().getString(R.string.text_entermobilenumber), PreferenceName.WARNING_MESSAGE);
            return;
        }
        button_submit.setEnabled(false);
        et_mobile.setEnabled(false);
        if (et_mobile.getText().toString().length() == 10) {
            sendOtp();
        } else {
            Toast.makeText(getApplicationContext(), "Please enter valid 10 digit phone number", Toast.LENGTH_SHORT).show();
            button_submit.setEnabled(true);
            et_mobile.setEnabled(true);
        }
    }

    private void sendOtp() {
        Utilz.showProgress(Login.this, getResources().getString(R.string.pleasewait));
        retrofitDataProvider.login("91" + et_mobile.getText().toString(), PreferenceName.USERROLE, new DownlodableCallback<UserDetailsModel>() {
            @Override
            public void onSuccess(final UserDetailsModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    startActivity(new Intent(Login.this, VerifyOtp.class).
                            putExtra("data", result.getNewUser()).
                            putExtra("otp", result.getOtp()).
                            putExtra("number", "91" + et_mobile.getText().toString()));
                    finish();

                }
            }

            @Override
            public void onFailure(String error) {
                Toast.makeText(getApplicationContext(), "Please check internetConection,Try again later", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(Login.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(Login.this, Login.class));
                finish();
            }
        });
    }

    private void initWidgit() {
        button_submit.setText(getResources().getString(R.string.text_submit));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onComplete() {

    }

}
