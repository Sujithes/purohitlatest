package bigappcompany.pujanpujari_purohit.map;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by shankar on 17/2/18.
 */

public class AddressResolverService extends JobIntentService {
    static final int JOB_ID = 1000;
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final String RECEIVER = ".RECEIVER";
    public static final String RESULT_DATA_KEY = ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = ".LOCATION_DATA_EXTRA";
    public static final String LOCATION_DATA_AREA = ".LOCATION_DATA_AREA";
    public static final String LOCATION_DATA_CITY = ".LOCATION_DATA_CITY";
    public static final String LOCATION_DATA_STREET = ".LOCATION_DATA_STREET";
    public static final String COUNTRYNAME = ".COUNTRY_NAME";
    public static final String LOCATION_POSTAL_CODE = ".POSTAL_CODE";
    private static final String TAG = "AddressResolverService";
    public static final String LAT = "latitute";
    public static final String LON = "lonitute";
    /**
     * The receiver where results are forwarded from this service.
     */
    protected ResultReceiver mReceiver;

    /**
     * This constructor is required, and calls the super IntentService(String)
     * constructor with the name for a worker thread.
     */
    public AddressResolverService() {
        // Use the TAG to name the worker thread.
        super();
    }

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, AddressResolverService.class, JOB_ID, work);
    }
    /**
     * Tries to get the location address using a Geocoder. If successful, sends an address to a
     * result receiver. If unsuccessful, sends an error message instead.
     * Note: We define a {@link ResultReceiver} in * MainActivity to process content
     * sent from this service.
     * <p>
     * This service calls this method from the default worker thread with the intent that started
     * the service. When this method returns, the service automatically stops.
     */
    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        String errorMessage = "";

        mReceiver = intent.getParcelableExtra(RECEIVER);

        // Check if receiver was properly registered.
        if (mReceiver == null) {
            Log.wtf(TAG, "No receiver received. There is nowhere to send the results.");
            return;
        }
        // Get the location passed to this service through an extra.
        Location location = intent.getParcelableExtra(LOCATION_DATA_EXTRA);

        // Make sure that the location data was really sent over through an extra. If it wasn't,
        // send an error error message and return.
        if (location == null) {
            errorMessage = "no_location_data_provided";
            Log.wtf(TAG, errorMessage);
            deliverResultToReceiver(FAILURE_RESULT, errorMessage, null);
            return;
        }

        // Errors could still arise from using the Geocoder (for example, if there is no
        // connectivity, or if the Geocoder is given illegal location data). Or, the Geocoder may
        // simply not have an address for a location. In all these cases, we communicate with the
        // receiver using a resultCode indicating failure. If an address is found, we use a
        // resultCode indicating success.

        // The Geocoder used in this sample. The Geocoder's responses are localized for the given
        // Locale, which represents a specific geographical or linguistic region. Locales are used
        // to alter the presentation of information such as numbers or dates to suit the conventions
        // in the region they describe.
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());

        // Address found using the Geocoder.
        List<Address> addresses = null;

        try {
            // Using getFromLocation() returns an array of Addresses for the area immediately
            // surrounding the given latitude and longitude. The results are a best guess and are
            // not guaranteed to be accurate.
            addresses = geocoder.getFromLocation(
                    location.getLatitude(),
                    location.getLongitude(),
                    // In this sample, we get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            errorMessage = "service_not_available";
            Log.e(TAG, errorMessage, ioException);
            getManualLocation(location.getLatitude(),
                    location.getLongitude());

        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            errorMessage = "invalid_lat_long_used";
            Log.e(TAG, errorMessage + ". " +
                    "Latitude = " + location.getLatitude() +
                    ", Longitude = " + location.getLongitude(), illegalArgumentException);
            getManualLocation(location.getLatitude(),
                    location.getLongitude());
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            if (errorMessage.isEmpty()) {
                errorMessage = "no_address_found";
                Log.e(TAG, errorMessage);
                getManualLocation(location.getLatitude(),
                        location.getLongitude());
            }
            deliverResultToReceiver(FAILURE_RESULT, errorMessage, null);
        } else {
            Address address = addresses.get(0);
            ArrayList<String> addressFragments = new ArrayList<>();

            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));

            }
            deliverResultToReceiver(SUCCESS_RESULT,
                    TextUtils.join(System.getProperty("line.separator"), addressFragments), address);
            //TextUtils.split(TextUtils.join(System.getProperty("line.separator"), addressFragments), System.getProperty("line.separator"));

        }
    }


    private void getManualLocation(double latitude, double longitude) {
        String address = String.format(Locale.ENGLISH, "http://maps.googleapis.com/maps/api/geocode/json?latlng=%1$f,%2$f&sensor=true&language=" + Locale.getDefault().getCountry(), latitude, longitude);


        HttpGet httpGet = new HttpGet(address);

        HttpClient client = new DefaultHttpClient();

        HttpResponse response;

        StringBuilder stringBuilder = new StringBuilder();


        List<Address> retList = new ArrayList<>();


        try {

            response = client.execute(httpGet);

            HttpEntity entity = response.getEntity();

            InputStream stream = entity.getContent();

            int b;

            while ((b = stream.read()) != -1) {

                stringBuilder.append((char) b);

            }


            JSONObject jsonObject = new JSONObject(stringBuilder.toString());

            retList = new ArrayList<>();


            if ("OK".equalsIgnoreCase(jsonObject.getString("status"))) {

                JSONArray results = jsonObject.getJSONArray("results");

                boolean hasGoodPartTwo = false;

                String city;
                String completeAddress = results.getJSONObject(0).getString("formatted_address");
                String lat = results.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getString("lat");
                String lon = results.getJSONObject(0).getJSONObject("geometry").getJSONObject("location").getString("lng");
                JSONArray array = results.getJSONObject(0).getJSONArray("address_components");
                if (array.length()>=4){
                    city = results.getJSONObject(0).getJSONArray("address_components").getJSONObject(4).getString("long_name");

                }
                else
                {
                    city = "";
                }
                //  for (int i = 0; i < results.length(); i++) {

                Bundle bundle = new Bundle();
                bundle.putString(LOCATION_DATA_CITY, city);
                bundle.putString(LOCATION_DATA_STREET, completeAddress);
                bundle.putDouble(LAT, Double.parseDouble(lat));
                bundle.putDouble(LON, Double.parseDouble(lon));
                mReceiver.send(SUCCESS_RESULT, bundle);
                // }
            }
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        catch (Exception e){

        }
    }




    /**
     * Sends a resultCode and message to the receiver.
     */
    private void deliverResultToReceiver(int resultCode, String message, Address address) {
        try {
            Bundle bundle = new Bundle();
            bundle.putString(RESULT_DATA_KEY, message);

            bundle.putString(LOCATION_DATA_AREA, address.getAdminArea());
            bundle.putString(LOCATION_POSTAL_CODE, address.getPostalCode());
            bundle.putString(LOCATION_DATA_CITY, address.getLocality());
            bundle.putString(LOCATION_DATA_STREET, address.getAddressLine(0));
            bundle.putString(COUNTRYNAME, address.getCountryName());
            bundle.putString(LAT, String.valueOf(address.getLatitude()));
            bundle.putString(LON, String.valueOf(address.getLongitude()));

            mReceiver.send(resultCode, bundle);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}