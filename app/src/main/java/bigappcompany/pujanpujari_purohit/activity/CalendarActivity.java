package bigappcompany.pujanpujari_purohit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.adapter.TimeSlotAdapter;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.customdatepicker.CalendarView;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.AvailabilityDetails;
import bigappcompany.pujanpujari_purohit.model.PurohitSlotAvalabilityModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.model.TimeSlotModel;
import bigappcompany.pujanpujari_purohit.model.TimeSlotResultModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shankar on 12/2/18.
 */

public class CalendarActivity extends AppCompatActivity {
    @BindView(R.id.calendar_view)
    CalendarView cv;
    @BindView(R.id.rv_timeslot)
    RecyclerView rv_timeslot;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.wholedate)
    CheckBox wholedate;
    @BindView(R.id.button_submit)
    Button button_submit;

    private String selectedDate;
    private boolean needtoclose = false;

    private RetrofitDataProvider retrofitDataProvider;
    private ArrayList<AvailabilityDetails> availableList = new ArrayList<>();
    private ArrayList<TimeSlotResultModel> timeSlotList = new ArrayList<>();
    private TimeSlotAdapter timeSlotAdapter;
    private ArrayList<String> id = new ArrayList<>();
    private int all_id[];

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calendar);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);

        domapping();
       // callSlotApi(); //callSlotAvailability();
        callSlotAvailability();
    }

    @OnClick(R.id.button_submit)
    void onButtonClick(){
        needtoclose = true;
        submitTimeSlot();
    }

    private void submitTimeSlot() {
        all_id = new int[id.size()];
        for (int i=0; i<id.size();i++){
            all_id[i] = Integer.valueOf(id.get(i));
        }

        Utilz.showProgress(CalendarActivity.this, getResources().getString(R.string.save_language));
        String sessiontoken = ClsGeneral.getPreferences(CalendarActivity.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(CalendarActivity.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.saveTimeSlot(sessiontoken, auth, all_id, selectedDate, new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    callSlotAvailability();
                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(CalendarActivity.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(CalendarActivity.this, Login.class));
                finish();
            }
        });
    }

    private void callSlotAvailability() {
        String sessiontoken = ClsGeneral.getPreferences(CalendarActivity.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(CalendarActivity.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.purohitTimeAvailable(sessiontoken, auth, selectedDate, new DownlodableCallback<PurohitSlotAvalabilityModel>() {
            @Override
            public void onSuccess(final PurohitSlotAvalabilityModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    timeSlotList.clear();
                    availableList.clear();
                    availableList = result.getResult().getAvailability_details();
                    callSlotApi();
                }
                else
                {
                    timeSlotList.clear();
                    availableList.clear();
                    Utilz.dismissProgress();
                    callSlotApi();
                }


            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(CalendarActivity.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(CalendarActivity.this, Login.class));
                finish();
            }
        });
    }

    private void callSlotApi() {
        String sessiontoken = ClsGeneral.getPreferences(CalendarActivity.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(CalendarActivity.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.purohitTimeSlot(sessiontoken, auth,  new DownlodableCallback<TimeSlotModel>() {
            @Override
            public void onSuccess(final TimeSlotModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    timeSlotList = result.getResult();
                    if (availableList.size()==0){
                        for (int i=0; i<timeSlotList.size();i++){
                            availableList.add(new AvailabilityDetails("0", false, timeSlotList.get(i).getId()));
                        }
                    }
                    else {
                        for (int i=0; i<timeSlotList.size();i++){
                            if (availableList.size()>i){

                            }
                            else {
                                availableList.add(new AvailabilityDetails("0", false, timeSlotList.get(i).getId()));
                            }
                        }
                    }

                    timeSlotAdapter = new TimeSlotAdapter(CalendarActivity.this, timeSlotList, availableList, R.layout.timeslot_row, new OnClickListener() {
                        @Override
                        public void onClick(int pos) {

                        }

                        @Override
                        public void twoParameter(String text1, String text2, int pos) {
                            if (text2.equalsIgnoreCase("add")){
                                id.add(text1);
                            }
                            else {
                                for (int i=0;i<id.size(); i++){
                                    if (id.get(i).equals(text1)){
                                        id.remove(i);
                                    }
                                }
                            }

                            checkListSize();

                        }
                    });
                    timeSlotAdapter.setchecked(false);
                    rv_timeslot.setAdapter(timeSlotAdapter);
                    if (needtoclose){
                        finish();
                    }
                }
                else
                {
                    Utilz.dismissProgress();
                }


            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(CalendarActivity.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(CalendarActivity.this, Login.class));
                finish();
            }
        });
    }

        private void checkListSize() {
            if (id.size()>0){
                button_submit.setVisibility(View.VISIBLE);
            }
            else {
                needtoclose = false;
                button_submit.setVisibility(View.GONE);
            }
    }


    private void domapping() {
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        button_submit.setText(getResources().getString(R.string.text_submit));
        button_submit.setVisibility(View.GONE);
        rv_timeslot.setLayoutManager(new GridLayoutManager(CalendarActivity.this, 3));
        rv_timeslot.setNestedScrollingEnabled(false);

        cv.setEventHandler(new CalendarView.EventHandler()
        {
            @Override
            public void onDayLongPress(String date)
            {
                selectedDate = date;
                callSlotAvailability();
            }
        });

        wholedate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                    for (int i=0; i<availableList.size();i++){
                        if (availableList.get(i).getAvailable_type().equals("0")){

                            availableList.get(i).setSelected(isChecked);
                        }
                    }

                    timeSlotAdapter.setchecked(true);
                    timeSlotAdapter.notifyDataSetChanged();
            }
        });

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c);
        selectedDate = formattedDate;
    }

}
