package bigappcompany.pujanpujari_purohit.dummy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import bigappcompany.pujanpujari_purohit.R;

/**
 * Created by shankar on 15/2/18.
 */

public class TestActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.leaverequest);
    }
}
