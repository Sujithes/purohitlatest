// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import com.mikhaellopez.circularimageview.CircularImageView;
import java.lang.IllegalStateException;
import java.lang.Override;

public class NewRequestAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private NewRequestAdapter.MyViewHolder target;

  @UiThread
  public NewRequestAdapter$MyViewHolder_ViewBinding(NewRequestAdapter.MyViewHolder target,
      View source) {
    this.target = target;

    target.iv_userImage = Utils.findRequiredViewAsType(source, R.id.iv_userImage, "field 'iv_userImage'", CircularImageView.class);
    target.tv_userName = Utils.findRequiredViewAsType(source, R.id.tv_userName, "field 'tv_userName'", TextView.class);
    target.tv_contact = Utils.findRequiredViewAsType(source, R.id.tv_contact, "field 'tv_contact'", TextView.class);
    target.tv_orderIdvalue = Utils.findRequiredViewAsType(source, R.id.tv_orderIdvalue, "field 'tv_orderIdvalue'", TextView.class);
    target.iv_product = Utils.findRequiredViewAsType(source, R.id.iv_product, "field 'iv_product'", CircularImageView.class);
    target.tv_productname = Utils.findRequiredViewAsType(source, R.id.tv_productname, "field 'tv_productname'", TextView.class);
    target.tv_date = Utils.findRequiredViewAsType(source, R.id.tv_date, "field 'tv_date'", TextView.class);
    target.tv_time = Utils.findRequiredViewAsType(source, R.id.tv_time, "field 'tv_time'", TextView.class);
    target.tv_languageName = Utils.findRequiredViewAsType(source, R.id.tv_languageName, "field 'tv_languageName'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    NewRequestAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_userImage = null;
    target.tv_userName = null;
    target.tv_contact = null;
    target.tv_orderIdvalue = null;
    target.iv_product = null;
    target.tv_productname = null;
    target.tv_date = null;
    target.tv_time = null;
    target.tv_languageName = null;
  }
}
