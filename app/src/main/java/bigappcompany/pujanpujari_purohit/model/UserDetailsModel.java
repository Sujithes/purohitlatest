package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 27/12/17.
 */

public class UserDetailsModel {

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getNewUser() {
        return newUser;
    }

    public void setNewUser(String newUser) {
        this.newUser = newUser;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public UserDetails getUser() {
        return user;
    }

    public void setUser(UserDetails user) {
        this.user = user;
    }
    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    @SerializedName("apiStatus")
    private String apiStatus;
    @SerializedName("newUser")
    private String newUser;
    @SerializedName("token")
    private String token;
    @SerializedName("otp")
    String otp;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @SerializedName("result")
    String result;
    @SerializedName("user")
    private UserDetails user;


}
