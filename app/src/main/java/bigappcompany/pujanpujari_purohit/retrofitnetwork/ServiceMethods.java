package bigappcompany.pujanpujari_purohit.retrofitnetwork;

import bigappcompany.pujanpujari_purohit.model.LanguageListModel;
import bigappcompany.pujanpujari_purohit.model.PujaListModel;
import bigappcompany.pujanpujari_purohit.model.PurohitDetailsModel;
import bigappcompany.pujanpujari_purohit.model.PurohitSlotAvalabilityModel;
import bigappcompany.pujanpujari_purohit.model.SignUpModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.model.TimeSlotModel;
import bigappcompany.pujanpujari_purohit.model.UserBookingListModel;
import bigappcompany.pujanpujari_purohit.model.UserDetailsModel;

public interface ServiceMethods {

    void login(String mobileNo, String role, DownlodableCallback<UserDetailsModel> callback);
    void verifyOtp(String mobile_number, String otp,String role, String device_token, DownlodableCallback<UserDetailsModel> callback);
    void languageList(String headertoken, String authorization, DownlodableCallback<LanguageListModel> callback);
    void signupPurohit(String first_name, String last_name,
                       String role, String email, String mobile_number, String address,  String latitude,
                       String longitude, String area_of_service, DownlodableCallback<SignUpModel> callback);
    void saveLanguage(String headertoken, String authorization, int[] language_id, DownlodableCallback<StatusResultModel> callback);
    void pujaListForPurohit(String headertoken, String authorization, DownlodableCallback<PujaListModel> callback);
    void savePujaService(String headertoken, String authorization, int[] puja_id, DownlodableCallback<StatusResultModel> callback);
    void purohitDetails(String headertoken, String authorization, DownlodableCallback<PurohitDetailsModel> callback);
    void purohitTimeSlot(String headertoken, String authorization, DownlodableCallback<TimeSlotModel> callback);
    void purohitTimeAvailable(String headertoken, String authorization, String date, DownlodableCallback<PurohitSlotAvalabilityModel> callback);
    void saveTimeSlot(String headertoken, String authorization, int[] timeslot_id, String date, DownlodableCallback<StatusResultModel> callback);
    void saveServiceDistance(String headertoken, String authorization,  String area_of_service, DownlodableCallback<StatusResultModel> callback);
    void acceptBooking(String headertoken, String authorization,  String request_id, DownlodableCallback<StatusResultModel> callback);
    void rejectBooking(String headertoken, String authorization,  String request_id, DownlodableCallback<StatusResultModel> callback);
    void userBookinList(String headertoken, String authorization,  String request_status, DownlodableCallback<UserBookingListModel> callback);
    void userBookedToCompleted(String headertoken, String authorization,  String booking_status, DownlodableCallback<UserBookingListModel> callback);
    void updateBooking(String headertoken, String authorization,  String booking_id, String booking_status, DownlodableCallback<StatusResultModel> callback);
    void updatePurohitAddressLocation(String headertoken, String authorization,  String address, String latitude, String longitude, DownlodableCallback<StatusResultModel> callback);
}
