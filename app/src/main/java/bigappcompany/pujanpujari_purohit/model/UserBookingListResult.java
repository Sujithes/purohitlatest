package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 20/3/18.
 */

public class UserBookingListResult {

    public String getPurohit_id() {
        return purohit_id;
    }

    public void setPurohit_id(String purohit_id) {
        this.purohit_id = purohit_id;
    }

    public String getRequest_status() {
        return request_status;
    }

    public void setRequest_status(String request_status) {
        this.request_status = request_status;
    }

    public String getBooking_date() {
        return booking_date;
    }

    public void setBooking_date(String booking_date) {
        this.booking_date = booking_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getLand_mark() {
        return land_mark;
    }

    public void setLand_mark(String land_mark) {
        this.land_mark = land_mark;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getTotal_amount() {
        return total_amount;
    }

    public void setTotal_amount(String total_amount) {
        this.total_amount = total_amount;
    }

    public String getAdvance_amount() {
        return advance_amount;
    }

    public void setAdvance_amount(String advance_amount) {
        this.advance_amount = advance_amount;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getBooking_status() {
        return booking_status;
    }

    public void setBooking_status(String booking_status) {
        this.booking_status = booking_status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPurohit_language() {
        return purohit_language;
    }

    public void setPurohit_language(String purohit_language) {
        this.purohit_language = purohit_language;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public String getPurohit_puja() {
        return purohit_puja;
    }

    public void setPurohit_puja(String purohit_puja) {
        this.purohit_puja = purohit_puja;
    }

    public String getPuja_id() {
        return puja_id;
    }

    public void setPuja_id(String puja_id) {
        this.puja_id = puja_id;
    }

    public UserBookingUserDetailsModel getUser_details() {
        return user_details;
    }

    public void setUser_details(UserBookingUserDetailsModel user_details) {
        this.user_details = user_details;
    }

    public ArrayList<UsetBookingTimeSlot> getTime_slot() {
        return time_slot;
    }

    public void setTime_slot(ArrayList<UsetBookingTimeSlot> time_slot) {
        this.time_slot = time_slot;
    }

    @SerializedName("purohit_id")
    String purohit_id;
    @SerializedName("request_status")
    String request_status;
    @SerializedName("booking_date")
    String booking_date;
    @SerializedName("address")
    String address;
    @SerializedName("address1")
    String address1;
    @SerializedName("land_mark")
    String land_mark;
    @SerializedName("latitude")
    String latitude;
    @SerializedName("longitude")
    String longitude;
    @SerializedName("total_amount")
    String total_amount;
    @SerializedName("advance_amount")
    String advance_amount;
    @SerializedName("payment_type")
    String payment_type;
    @SerializedName("payment_status")
    String payment_status;
    @SerializedName("booking_status")
    String booking_status;
    @SerializedName("id")
    String id;
    @SerializedName("purohit_language")
    String purohit_language;
    @SerializedName("language_id")
    String language_id;
    @SerializedName("purohit_puja")
    String purohit_puja;
    @SerializedName("puja_id")
    String puja_id;
    @SerializedName("user_details")
    UserBookingUserDetailsModel user_details;
    @SerializedName("time_slot")
    ArrayList<UsetBookingTimeSlot> time_slot;

    public String getPaid_amount() {
        return paid_amount;
    }

    public void setPaid_amount(String paid_amount) {
        this.paid_amount = paid_amount;
    }

    public String getPending_amount() {
        return pending_amount;
    }

    public void setPending_amount(String pending_amount) {
        this.pending_amount = pending_amount;
    }

    @SerializedName("paid_amount")
    String paid_amount;
    @SerializedName("pending_amount")
    String pending_amount;
}
