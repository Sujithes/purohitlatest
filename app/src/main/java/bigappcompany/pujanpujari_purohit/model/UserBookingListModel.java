package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 20/3/18.
 */

public class UserBookingListModel {

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public ArrayList<UserBookingListResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<UserBookingListResult> result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    ArrayList<UserBookingListResult> result;
}
