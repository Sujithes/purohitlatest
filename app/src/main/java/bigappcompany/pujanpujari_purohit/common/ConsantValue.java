package bigappcompany.pujanpujari_purohit.common;

/**
 * Created by shankar on 30/12/17.
 */

public class ConsantValue {

    public static final String BASE_URL = "https://pujanpujari.com:2020/";
    public static final String IMAGE_URL = "http://13.127.74.60:2030";
    public static final String API_KEY = "PM4dimje2lfbIbJG0KzN";
    public static final String LOGINWITHMOBILENUMBER = "otpGeneration";
    public static final String VERIFYOTP = "loginCheckViaOTP";
    public static final String LISTSHOPBANNER = "listBannerForUser";
    public static final String LISTLANGUAGE = "listLanguageForPurohit";
    public static final String SIGHNUP = "siginupPurohit";
    public static final String ADDPUROHITLANGUAGE = "purohitLanguage";
    public static final String PUJALISTFORPUROHIT = "listPujaForPurohit";
    public static final String ADDPUROHITPUJA = "purohitPuja";
    public static final String PUROHTDETAILS = "listPurohitDetails";
    public static final String PUROHITTIMESLOT = "listTimeSlotForUser";
    public static final String SLOTAVAILABLE = "listAvailabilityForPurohit";
    public static final String SETTIMESLOT = "setBusy";
    public static final String UPDATEPUROHITDETAILS = "updatePurohitDetails";
    public static final String ACCEPTBOOKING = "acceptBooking";
    public static final String REJECTBOOKING = "rejectBooking";
    public static final String USERBOOKINGLIST = "listBookingDetailsForPurohit";
    public static final String USERBOOKEDLIST = "listBookedDetailsForPurohit";
    public static final String UPDATEBOOKING = "updateBooking";
}
