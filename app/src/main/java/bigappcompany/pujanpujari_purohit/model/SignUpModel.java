package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 8/3/18.
 */

public class SignUpModel {
    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public SignupUserModel getUser() {
        return user;
    }

    public void setUser(SignupUserModel user) {
        this.user = user;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("token")
    String token;
    @SerializedName("user")
    SignupUserModel user;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    @SerializedName("result")
    String result;
}
