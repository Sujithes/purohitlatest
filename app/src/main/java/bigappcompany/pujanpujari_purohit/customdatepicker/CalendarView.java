package bigappcompany.pujanpujari_purohit.customdatepicker;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;

import bigappcompany.pujanpujari_purohit.R;
import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Created by a7med on 28/06/2015.
 */
public class CalendarView extends LinearLayout
{
	// for logging
	private static final String LOGTAG = "Calendar View";
	private  Context context;

	// how many days to show, defaults to six weeks, 42 days
	private static final int DAYS_COUNT = 42;

	// default date format
	private static final String DATE_FORMAT = "MMM yyyy";

	// date format
	private String dateFormat;

	// current displayed month
	private Calendar currentDate = Calendar.getInstance();

	//event handling
	private EventHandler eventHandler = null;

	// internal components
	private LinearLayout header;
	private ImageView btnPrev;
	private ImageView btnNext;
	private TextView txtDate;
	private RecyclerView grid;

	// seasons' rainbow
	int[] rainbow = new int[] {
			R.color.summer,
			R.color.fall,
			R.color.winter,
			R.color.spring
	};

	// month-season association (northern hemisphere, sorry australia :)
	int[] monthSeason = new int[] {2, 2, 3, 3, 3, 0, 0, 0, 1, 1, 1, 2};

	public CalendarView(Context context)
	{
		super(context);
	}

	public CalendarView(Context context, AttributeSet attrs)
	{
		super(context, attrs);
		this.context = context;
		initControl(context, attrs);
	}

	public CalendarView(Context context, AttributeSet attrs, int defStyleAttr)
	{
		super(context, attrs, defStyleAttr);
		initControl(context, attrs);
	}

	/**
	 * Load control xml layout
	 */
	private void initControl(Context context, AttributeSet attrs)
	{
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.control_calendar, this);

		loadDateFormat(attrs);
		assignUiElements();
		assignClickHandlers();

		updateCalendar();
	}

	private void loadDateFormat(AttributeSet attrs)
	{
		TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.CalendarView);

		try
		{
			// try to load provided date format, and fallback to default otherwise
			dateFormat = ta.getString(R.styleable.CalendarView_dateFormat);
			if (dateFormat == null)
				dateFormat = DATE_FORMAT;
		}
		finally
		{
			ta.recycle();
		}
	}
	private void assignUiElements()
	{
		// layout is inflated, assign local variables to components
		header = (LinearLayout)findViewById(R.id.calendar_header);
		btnPrev = (ImageView)findViewById(R.id.calendar_prev_button);
		btnNext = (ImageView)findViewById(R.id.calendar_next_button);
		txtDate = (TextView)findViewById(R.id.calendar_date_display);
		grid = (RecyclerView) findViewById(R.id.calendar_grid);
	}

	private void assignClickHandlers()
	{
		// add one month and refresh UI
		btnNext.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				currentDate.add(Calendar.MONTH, 1);
				updateCalendar();
			}
		});

		// subtract one month and refresh UI
		btnPrev.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v)
			{
				currentDate.add(Calendar.MONTH, -1);
				updateCalendar();
			}
		});
	}

	/**
	 * Display dates correctly in grid
	 */
	public void updateCalendar()
	{
		updateCalendar(null);
	}

	/**
	 * Display dates correctly in grid
	 */
	public void updateCalendar(HashSet<Date> events)
	{

		ArrayList<Date> cells = new ArrayList<>();
		Calendar calendar = (Calendar)currentDate.clone();

		// determine the cell for current month's beginning
		calendar.set(Calendar.DAY_OF_MONTH, 2);
		int monthBeginningCell = calendar.get(Calendar.DAY_OF_WEEK) - 1;

		// move calendar backwards to the beginning of the week
		calendar.add(Calendar.DAY_OF_MONTH, -monthBeginningCell);

		// fill cells
		while (cells.size() < DAYS_COUNT)
		{
			cells.add(calendar.getTime());
			calendar.add(Calendar.DAY_OF_MONTH, 1);
		}

		// update grid
		grid.setNestedScrollingEnabled(false);
		grid.setLayoutManager(new GridLayoutManager(context,7));
		grid.setAdapter(new CalendarAdapter(getContext(), cells, events));

		// update title
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		txtDate.setText(sdf.format(currentDate.getTime()));

		// set header color according to current season
		int month = currentDate.get(Calendar.MONTH);
		int season = monthSeason[month];
		int color = rainbow[season];

		//header.setBackgroundColor(getResources().getColor(color));
	}


	class CalendarAdapter extends RecyclerView.Adapter<CalendarAdapter.MyViewHolder> {

		// days with events
		private HashSet<Date> eventDays;

		// for view inflation
		private LayoutInflater inflater;
		ArrayList<Date> days;
		private int dummydate,dummymonth;
		private boolean clicked = false;

		public class MyViewHolder extends RecyclerView.ViewHolder {
			@BindView(R.id.dateText) TextView dateText;

			public MyViewHolder(View view) {
				super(view);
				ButterKnife.bind(this,view);
			}
		}

		public CalendarAdapter(Context context, ArrayList<Date> days, HashSet<Date> eventDays)
		{
			this.eventDays = eventDays;
			this.days = days;
			inflater = LayoutInflater.from(context);
		}

		@Override
		public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
			View itemView = LayoutInflater.from(parent.getContext())
					.inflate(R.layout.control_calendar_day, parent, false);

			return new MyViewHolder(itemView);



		}

		@Override
		public void onBindViewHolder(final MyViewHolder holder, final int position) {
			// day in question
			final Date date = days.get(position);
			int day = date.getDate();
			if (!clicked) dummydate = date.getDate();
			if (!clicked) dummymonth = date.getMonth();
			int month = date.getMonth();
			int year = date.getYear();

			// today
			Date today = new Date();
			// if this day has an event, specify event image
			//	ImageView image = (ImageView) view.findViewById(R.id.image);
			if (eventDays != null)
			{
				for (Date eventDate : eventDays)
				{
					if (eventDate.getDate() == day &&
							eventDate.getMonth() == month &&
							eventDate.getYear() == year)
					{
						break;
					}
				}
			}

			// clear styling

			Log.e("Month : Today's Month", ""+month+" : "+today.getMonth()) ;
			Log.e("Year : Today year", ""+year+" : "+today.getYear()) ;
			Log.e("todays date", ""+(date.getDate()+","+(date.getMonth()+1)+","+(1900+date.getYear())));
			Log.e("todays current date", (today.getDate()+","+today.getMonth()+","+today.getYear()));
			if (month < today.getMonth() && year <= today.getYear())
			{
			}
			else if (!clicked && (dummydate == today.getDate() && month == today.getMonth()))
			{
				holder.dateText.setBackgroundResource(R.drawable.circular_textviw);
			}
			if (clicked && (dummydate == date.getDate() && dummymonth == date.getMonth()))
			{
				holder.dateText.setBackgroundResource(R.drawable.circular_textviw);
			}


			// set text
			holder.dateText.setText(String.valueOf(date.getDate()));

			holder.itemView.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						clicked = true;
						dummydate = date.getDate();
						dummymonth = date.getMonth();
						String selected_date = getdate(date);
						eventHandler.onDayLongPress(selected_date);
						notifyDataSetChanged();
					}
					catch (NullPointerException e)
					{
                       e.printStackTrace();
					}
				}
			});

		}

		@Override
		public int getItemCount() {
			return days.size();
		}
	}

	private String getdate(Date date) {
		int a = date.getMonth()+1;
		int b = date.getDate();
		String dte = (1900+date.getYear())+"-"+((a < 10 ? "0" : "") + a)+"-"+(b < 10 ? "0" : "") + b;
		return dte;
	}

	/**
	 * Assign event handler to be passed needed events
	 */
	public void setEventHandler(EventHandler eventHandler)
	{
		this.eventHandler = eventHandler;
	}

	/**
	 * This interface defines what events to be reported to
	 * the outside world
	 */
	public interface EventHandler
	{
		void onDayLongPress(String date);
	}
}