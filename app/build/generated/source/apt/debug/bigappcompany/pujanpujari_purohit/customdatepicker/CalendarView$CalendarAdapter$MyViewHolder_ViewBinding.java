// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.customdatepicker;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CalendarView$CalendarAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private CalendarView.CalendarAdapter.MyViewHolder target;

  @UiThread
  public CalendarView$CalendarAdapter$MyViewHolder_ViewBinding(CalendarView.CalendarAdapter.MyViewHolder target,
      View source) {
    this.target = target;

    target.dateText = Utils.findRequiredViewAsType(source, R.id.dateText, "field 'dateText'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    CalendarView.CalendarAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.dateText = null;
  }
}
