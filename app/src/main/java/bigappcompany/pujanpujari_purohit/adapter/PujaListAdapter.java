package bigappcompany.pujanpujari_purohit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import java.util.ArrayList;
import java.util.List;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.model.PujaListResultModel;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shankar on 8/3/18.
 */

public class PujaListAdapter extends RecyclerView.Adapter<PujaListAdapter.MyViewHolder> {

    private List<PujaListResultModel> languageListModels;
    private Context context;
    private int categorylist_row;
    private OnClickListener onClickListener ;
    ArrayList<String> id;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.languageCheck)
        CheckBox languageCheck;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }


    public PujaListAdapter(Context context, ArrayList<PujaListResultModel> languageListModels, ArrayList<String> id, int categorylist_row, OnClickListener onClickListener) {
        this.context = context;
        this.languageListModels = languageListModels;
        this.categorylist_row = categorylist_row;
        this.onClickListener = onClickListener;
        this.id = id;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(categorylist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final int pos = position;
        final PujaListResultModel pl = languageListModels.get(position);

        holder.languageCheck.setText(pl.getPuja_name());
        holder.languageCheck.setOnCheckedChangeListener(null);
        if (id.size()>0){
            for (int i=0; i<id.size();i++){
                if (pl.getId().equals(id.get(i))) {
                    pl.setSelected(true);
                }

            }
        }

        holder.languageCheck.setTag(pl);

        holder.languageCheck.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                PujaListResultModel contact = (PujaListResultModel) cb.getTag();

                contact.setSelected(cb.isChecked());
                languageListModels.get(pos).setSelected(cb.isChecked());

                if (cb.isChecked()){
                    onClickListener.twoParameter(languageListModels.get(pos).getPuja_name(),"add", pos);
                }
                else {
                    onClickListener.twoParameter(languageListModels.get(pos).getPuja_name(),"remove", pos);
                }

            }
        });
        /*holder.languageCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    pl.setSelected(true);
                    onClickListener.twoParameter(languageListModels.get(pos).getPuja_name(),"add", pos);
                }else {
                    pl.setSelected(false);
                    onClickListener.twoParameter(languageListModels.get(pos).getPuja_name(),"remove", pos);
                }
            }
        });*/
        holder.languageCheck.setChecked(pl.isSelected());


    }

    @Override
    public int getItemCount() {
        return languageListModels.size();
    }
}


