package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 20/3/18.
 */

public class UsetBookingTimeSlot {
    public String getPurohit_booking_id() {
        return purohit_booking_id;
    }

    public void setPurohit_booking_id(String purohit_booking_id) {
        this.purohit_booking_id = purohit_booking_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public UserBookingSlotId getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(UserBookingSlotId slot_id) {
        this.slot_id = slot_id;
    }

    @SerializedName("purohit_booking_id")
    String purohit_booking_id;
    @SerializedName("id")
    String id;
    @SerializedName("createdAt")
    String createdAt;
    @SerializedName("updatedAt")
    String updatedAt;
    @SerializedName("slot_id")
    UserBookingSlotId slot_id;
}
