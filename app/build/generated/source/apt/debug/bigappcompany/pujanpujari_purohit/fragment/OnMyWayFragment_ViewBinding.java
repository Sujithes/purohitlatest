// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.fragment;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class OnMyWayFragment_ViewBinding implements Unbinder {
  private OnMyWayFragment target;

  @UiThread
  public OnMyWayFragment_ViewBinding(OnMyWayFragment target, View source) {
    this.target = target;

    target.common_recyclerview = Utils.findRequiredViewAsType(source, R.id.common_recyclerview, "field 'common_recyclerview'", RecyclerView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    OnMyWayFragment target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.common_recyclerview = null;
  }
}
