package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 19/2/18.
 */

public class LanguageListModel  {
    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public ArrayList<LanguageResult> getResult() {
        return result;
    }

    public void setResult(ArrayList<LanguageResult> result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    ArrayList<LanguageResult> result;
}
