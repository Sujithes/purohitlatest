// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.login;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class VerifyOtp_ViewBinding implements Unbinder {
  private VerifyOtp target;

  private View view2131296323;

  @UiThread
  public VerifyOtp_ViewBinding(VerifyOtp target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public VerifyOtp_ViewBinding(final VerifyOtp target, View source) {
    this.target = target;

    View view;
    target.et_otp = Utils.findRequiredViewAsType(source, R.id.et_otp, "field 'et_otp'", EditText.class);
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'onButtonClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick();
      }
    });
    target.tv_resend = Utils.findRequiredViewAsType(source, R.id.tv_resend, "field 'tv_resend'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    VerifyOtp target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.et_otp = null;
    target.button_submit = null;
    target.tv_resend = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
  }
}
