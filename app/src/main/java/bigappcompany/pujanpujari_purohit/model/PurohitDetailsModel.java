package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 9/3/18.
 */

public class PurohitDetailsModel {
    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public PurohitDetailsResultModel getResult() {
        return result;
    }

    public void setResult(PurohitDetailsResultModel result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    PurohitDetailsResultModel result;
}
