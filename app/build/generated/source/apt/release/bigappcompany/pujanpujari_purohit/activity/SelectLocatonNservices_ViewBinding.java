// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class SelectLocatonNservices_ViewBinding implements Unbinder {
  private SelectLocatonNservices target;

  private View view2131296456;

  private View view2131296680;

  private View view2131296690;

  @UiThread
  public SelectLocatonNservices_ViewBinding(SelectLocatonNservices target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public SelectLocatonNservices_ViewBinding(final SelectLocatonNservices target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.iv_back, "field 'iv_back' and method 'onBackClick'");
    target.iv_back = Utils.castView(view, R.id.iv_back, "field 'iv_back'", ImageView.class);
    view2131296456 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onBackClick();
      }
    });
    view = Utils.findRequiredView(source, R.id.tv_locationEdit, "field 'tv_locationEdit' and method 'onEditClick'");
    target.tv_locationEdit = Utils.castView(view, R.id.tv_locationEdit, "field 'tv_locationEdit'", TextView.class);
    view2131296680 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onEditClick(Utils.castParam(p0, "doClick", 0, "onEditClick", 0, TextView.class));
      }
    });
    target.tv_locationName = Utils.findRequiredViewAsType(source, R.id.tv_locationName, "field 'tv_locationName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.tv_seekbarEdit, "field 'tv_seekbarEdit' and method 'onEditClick'");
    target.tv_seekbarEdit = Utils.castView(view, R.id.tv_seekbarEdit, "field 'tv_seekbarEdit'", TextView.class);
    view2131296690 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onEditClick(Utils.castParam(p0, "doClick", 0, "onEditClick", 0, TextView.class));
      }
    });
    target.seekrange = Utils.findRequiredViewAsType(source, R.id.seekrange, "field 'seekrange'", TextView.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    SelectLocatonNservices target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.iv_back = null;
    target.tv_locationEdit = null;
    target.tv_locationName = null;
    target.tv_seekbarEdit = null;
    target.seekrange = null;

    view2131296456.setOnClickListener(null);
    view2131296456 = null;
    view2131296680.setOnClickListener(null);
    view2131296680 = null;
    view2131296690.setOnClickListener(null);
    view2131296690 = null;
  }
}
