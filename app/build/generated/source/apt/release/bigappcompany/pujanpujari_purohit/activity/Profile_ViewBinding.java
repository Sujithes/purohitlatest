// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class Profile_ViewBinding implements Unbinder {
  private Profile target;

  private View view2131296680;

  private View view2131296562;

  private View view2131296564;

  private View view2131296323;

  @UiThread
  public Profile_ViewBinding(Profile target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public Profile_ViewBinding(final Profile target, View source) {
    this.target = target;

    View view;
    view = Utils.findRequiredView(source, R.id.tv_locationEdit, "field 'tv_locationEdit' and method 'onEditClick'");
    target.tv_locationEdit = Utils.castView(view, R.id.tv_locationEdit, "field 'tv_locationEdit'", TextView.class);
    view2131296680 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onEditClick();
      }
    });
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.mToolbar, "field 'mToolbar'", Toolbar.class);
    target.purohitName = Utils.findRequiredViewAsType(source, R.id.purohitName, "field 'purohitName'", TextView.class);
    view = Utils.findRequiredView(source, R.id.purohitLanguage, "field 'purohitLanguage' and method 'TextClicked'");
    target.purohitLanguage = Utils.castView(view, R.id.purohitLanguage, "field 'purohitLanguage'", TextView.class);
    view2131296562 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.TextClicked(Utils.castParam(p0, "doClick", 0, "TextClicked", 0, TextView.class));
      }
    });
    target.userMobile = Utils.findRequiredViewAsType(source, R.id.userMobile, "field 'userMobile'", TextView.class);
    target.purohitEmail = Utils.findRequiredViewAsType(source, R.id.purohitEmail, "field 'purohitEmail'", TextView.class);
    view = Utils.findRequiredView(source, R.id.purohitPujaServices, "field 'purohitPujaServices' and method 'TextClicked'");
    target.purohitPujaServices = Utils.castView(view, R.id.purohitPujaServices, "field 'purohitPujaServices'", TextView.class);
    view2131296564 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.TextClicked(Utils.castParam(p0, "doClick", 0, "TextClicked", 0, TextView.class));
      }
    });
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'onButtonClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    Profile target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.tv_locationEdit = null;
    target.mToolbar = null;
    target.purohitName = null;
    target.purohitLanguage = null;
    target.userMobile = null;
    target.purohitEmail = null;
    target.purohitPujaServices = null;
    target.button_submit = null;

    view2131296680.setOnClickListener(null);
    view2131296680 = null;
    view2131296562.setOnClickListener(null);
    view2131296562 = null;
    view2131296564.setOnClickListener(null);
    view2131296564 = null;
    view2131296323.setOnClickListener(null);
    view2131296323 = null;
  }
}
