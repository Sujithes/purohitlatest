package bigappcompany.pujanpujari_purohit.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 20/2/18.
 */

public class ServiceRadius extends BaseActivity implements OnMapReadyCallback,SeekBar.OnSeekBarChangeListener{
    @BindView(R.id.button_submit)
    Button button_submit;
    @BindView(R.id.seekBar1)
    SeekBar seekBar1;
    @BindView(R.id.iv_back)
    ImageView iv_back;
    @BindView(R.id.seekrange)
    TextView seekrange;

    private static final String TAG = "MapActivity";
    private GoogleMap mMap;
    private LatLng latLng;
    private boolean seek = false;
    private Circle circle = null;
    private Marker mMarker;
    private int progressrange;
    private RetrofitDataProvider retrofitDataProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_serviceradius);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);
        initWidgit();
    }



    private void initWidgit() {

        button_submit.setText(getResources().getString(R.string.done));
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        seekBar1.setOnSeekBarChangeListener(this);

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setAllGesturesEnabled(false);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
        googleMap.addMarker(new MarkerOptions().position(latLng)
                .title("Your Location"));
        drawCircle(1);



    }




    private void drawCircle(int progress) {
        if(circle!=null){
            circle.remove();
        }
        circle = mMap.addCircle(new CircleOptions()
                .center(new LatLng(latLng.latitude, latLng.longitude))
                .radius(1000*progress)
                .strokeWidth(5)
                .strokeColor(Color.RED)
                .fillColor(Color.TRANSPARENT));
    }


    @Override
    public void onProgressChanged(SeekBar seekBar, int progress,
                                  boolean fromUser) {
        float zoomvalue =0;
        seek = true;


        if (progress==10){
            zoomvalue = 9.5f;
        }else if (progress==9){
            zoomvalue = 9.9f;
        }else if (progress==8){
            zoomvalue = 10.0f;
        }else if (progress==7){
            zoomvalue = 10.2f;
        }else if (progress==6){
            zoomvalue = 10.4f;
        }else if (progress==5){
            zoomvalue = 10.6f;
        }else if (progress==4){
            zoomvalue = 11.0f;
        }else if (progress==3){
            zoomvalue = 11.4f;
        }else if (progress==2){
            zoomvalue = 11.9f;
        }else if (progress==1){
            zoomvalue = 12.9f;
        }else if (progress==0){
            zoomvalue = 13.0f;
            progress = 1;
        }

        if (seek) {
            seekrange.setText("" + (progress) + " km");
            progressrange = progress;
        }
        else {
            seekrange.setText("" + (progress) + " km");
            progressrange = progress;
        }
        seek = true;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoomvalue));
        drawCircle(progress);

    }
    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {


    }
    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        // Toast.makeText(getApplicationContext(),"seekbar touch stopped!", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }



    @Override
    protected void onResume() {
        super.onResume();

        Double lat = null,lon = null;
        try {
            lat = Double.parseDouble(ClsGeneral.getPreferences(ServiceRadius.this, PreferenceName.LATITUTE));
            lon = Double.parseDouble(ClsGeneral.getPreferences(ServiceRadius.this, PreferenceName.LONGITUTE));
        }
        catch (NumberFormatException e){

            lat = 13.027231;
            lon = 77.577572;
        }
        catch (Exception e){
            lat = 13.027231;
            lon = 77.577572;
        }
        latLng = new LatLng(lat,lon);
        String seekbarrange = ClsGeneral.getPreferences(ServiceRadius.this, PreferenceName.SEEKRANGE);
        try {
            seekBar1.setProgress(Integer.parseInt(seekbarrange));
            seekrange.setText(seekbarrange+" km");
        }catch (NumberFormatException e){
           /* seekBar1.setProgress(1);
            seekrange.setText("2 km");*/
        }catch (Exception e){

        }

        try{
            if (mMap!=null){
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 13.0f));
                mMap.addMarker(new MarkerOptions().position(latLng)
                        .title("Your Location"));
            }

        }
        catch (NullPointerException e)
        {
            e.printStackTrace();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @OnClick(R.id.button_submit)
    void obSubmitClick(){
        if (getIntent().getStringExtra("from").equals("other")){

            saveServiceDistance();
        }
        else {
            setResultngoBack();
        }

    }

    private void saveServiceDistance() {
        Utilz.showProgress(ServiceRadius.this, getResources().getString(R.string.pleasewait));
        String sessiontoken = ClsGeneral.getPreferences(ServiceRadius.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(ServiceRadius.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.saveServiceDistance(sessiontoken,auth, ""+progressrange,new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    ClsGeneral.setPreferences(ServiceRadius.this, PreferenceName.SEEKRANGE, ""+progressrange);
                    finish();

                }
                else
                {

                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {
                ClsGeneral.setPreferences(ServiceRadius.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(ServiceRadius.this, Login.class));
                finish();
            }
        });
    }

    private void setResultngoBack() {
        //ClsGeneral.setPreferences(ServiceRadius.this, PreferenceName.SEEKRANGE, ""+progressrange);
        if (getIntent().getStringExtra("from").equals("signup")){
            String message= ""+progressrange;
            Intent intent=new Intent();
            intent.putExtra("MESSAGE",message);
            setResult(3,intent);
            finish();
        }
        else {
            finish();
        }
    }


    @OnClick(R.id.iv_back)
    void onBackClick(){
        setResultngoBack();
    }

    @Override
    public void onBackPressed() {

    }
}
