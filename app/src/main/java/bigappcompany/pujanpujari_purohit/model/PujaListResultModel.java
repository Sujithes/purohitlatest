package bigappcompany.pujanpujari_purohit.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 8/3/18.
 */

public class PujaListResultModel implements Parcelable{

    protected PujaListResultModel(Parcel in) {
        puja_name = in.readString();
        id = in.readString();
        puja_image = in.readString();
        short_description = in.readString();
        selected = in.readByte() != 0;
    }

    public static final Creator<PujaListResultModel> CREATOR = new Creator<PujaListResultModel>() {
        @Override
        public PujaListResultModel createFromParcel(Parcel in) {
            return new PujaListResultModel(in);
        }

        @Override
        public PujaListResultModel[] newArray(int size) {
            return new PujaListResultModel[size];
        }
    };

    public String getPuja_name() {
        return puja_name;
    }

    public void setPuja_name(String puja_name) {
        this.puja_name = puja_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPuja_image() {
        return puja_image;
    }

    public void setPuja_image(String puja_image) {
        this.puja_image = puja_image;
    }

    public String getShort_description() {
        return short_description;
    }

    public void setShort_description(String short_description) {
        this.short_description = short_description;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @SerializedName("puja_name")
    String puja_name;
    @SerializedName("id")
    String id;
    @SerializedName("puja_image")
    String puja_image;
    @SerializedName("short_description")
    String short_description;
    boolean selected;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(puja_name);
        dest.writeString(id);
        dest.writeString(puja_image);
        dest.writeString(short_description);
        dest.writeByte((byte) (selected ? 1 : 0));
    }
}
