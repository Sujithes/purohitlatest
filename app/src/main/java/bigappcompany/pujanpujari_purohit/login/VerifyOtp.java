package bigappcompany.pujanpujari_purohit.login;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.activity.BaseActivity;
import bigappcompany.pujanpujari_purohit.activity.MainActivity;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.model.UserDetailsModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.ProgressGenerator;
import bigappcompany.pujanpujari_purohit.utilz.SmsBroadcastReceiver;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by shankar on 9/2/18.
 */

public class VerifyOtp extends BaseActivity implements ProgressGenerator.OnCompleteListener{
    @BindView(R.id.et_otp)
    EditText et_otp;
    @BindView(R.id.button_submit)
    Button button_submit;
    @BindView(R.id.tv_resend)
    TextView tv_resend;

    private String otp;


    private SmsBroadcastReceiver readsms;
    private IntentFilter intentFilter;
    private RetrofitDataProvider retrofitDataProvider;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_verifyotp);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);
        checkPermissionAndGetSms();
        initWidgit();
        settingSpannable();

    }

    private void settingSpannable() {
        String content = getResources().getString(R.string.text_wesentuotp);
        SpannableString ss = new SpannableString(content);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                sendOtp();
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
                ds.setUnderlineText(true);
                ds.setColor(getResources().getColor(R.color.colorAccent));
            }
        };
        ss.setSpan(clickableSpan, content.length()-7, content.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tv_resend.setText(ss);
        tv_resend.setMovementMethod(LinkMovementMethod.getInstance());
        tv_resend.setBackgroundColor(getResources().getColor(R.color.transparent));
        tv_resend.setHighlightColor(getResources().getColor(R.color.transparent));

    }

    private void sendOtp() {
       // progressGenerator.start(btnSignIn);
        Utilz.showProgress(VerifyOtp.this, getResources().getString(R.string.pleasewait));
        retrofitDataProvider.login(getIntent().getStringExtra("number"), PreferenceName.USERROLE, new DownlodableCallback<UserDetailsModel>() {
            @Override
            public void onSuccess(final UserDetailsModel result) {
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    Utilz.dismissProgress();
                    otp = result.getOtp();

                }
                else
                {

                }
            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                                ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_ID, "");
                                startActivity(new Intent(VerifyOtp.this, Login.class));
                                finish();
            }
        });
    }

    @OnClick(R.id.button_submit)
    void onButtonClick() {

        et_otp.setEnabled(true);
        String otpvalue = null;
        try {
             otpvalue = getIntent().getStringExtra("data");
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        if (otpvalue.equals("true")){

            if (et_otp.getText().toString().equals(otp)){
                startActivity(new Intent(VerifyOtp.this, SignUp.class).
                putExtra("number",getIntent().getStringExtra("number")));
                finish();

            }
            else {
                Utilz.amazingMessage(VerifyOtp.this, getResources().getString(R.string.text_correctotp), PreferenceName.WARNING_MESSAGE);
                et_otp.setEnabled(true);
            }
        }
        else {
            if (et_otp.getText().toString().equals(otp)) {
                verifyOtp();
            }
        }

    }

    private void verifyOtp() {
        Utilz.showProgress(VerifyOtp.this, getResources().getString(R.string.pleasewait));
        String token = ClsGeneral.getPreferences(VerifyOtp.this, PreferenceName.DEVICE_TOKEN);
        retrofitDataProvider.verifyOtp(getIntent().getStringExtra("number"), getIntent().getStringExtra("otp"), PreferenceName.USERROLE, token, new DownlodableCallback<UserDetailsModel>() {
            @Override
            public void onSuccess(final UserDetailsModel result) {

                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_ID, result.getUser().getId());
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_MOBILE, result.getUser().getMobile_number());

                        try
                        {
                            ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_TOKEN, result.getToken());
                            ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_SESSION_TOKEN, result.getUser().getSession_token());
                        }
                        catch (Exception e)
                        {
                            e.printStackTrace();
                        }
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_EMAIL, result.getUser().getEmail());
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_NAME, result.getUser().getUser_name());
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_ADDRESS, result.getUser().getAddress());
                        ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_IMAGE, result.getUser().getProfile_image());

                        startActivity(new Intent(VerifyOtp.this, MainActivity.class));
                        finish();
                }
                else
                {
                    showVerifyDialog(result.getResult());
                    Utilz.amazingMessage(VerifyOtp.this, result.getResult(), PreferenceName.NORMAL_MESSAGE);
                }


            }

            @Override
            public void onFailure(String error) {
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                                ClsGeneral.setPreferences(VerifyOtp.this, PreferenceName.USER_ID, "");
                                startActivity(new Intent(VerifyOtp.this, Login.class));
                                finish();
            }
        });
    }

    private void showVerifyDialog(String result) {
        AlertDialog alertDialog = new AlertDialog.Builder(
                VerifyOtp.this).create();

        // Setting Dialog Title
        alertDialog.setTitle("Puja n Pujari");

        // Setting Dialog Message
        alertDialog.setMessage(result);

        // Setting Icon to Dialog
        alertDialog.setIcon(R.mipmap.location_icon);

        // Setting OK Button
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    private void initWidgit() {
        otp = getIntent().getStringExtra("otp");
        button_submit.setText(getResources().getString(R.string.text_submit));
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onComplete() {

    }

    private void checkPermissionAndGetSms() {
        readsms = new SmsBroadcastReceiver() {
            @Override
            protected void onSmsReceived(String s) {
                if (s != null && s.length() > 0) {
                    if (s.contains("pujaNpujari")) {
                        String splitmsg[] = s.split(" ");
                        otp = splitmsg[6];
                        et_otp.setText(otp);
                    }
                }
            }
        };

        intentFilter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        intentFilter.setPriority(1000);

        if (Build.VERSION.SDK_INT >= 23) {
            checkLocationPermission();
        }
    }

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private void checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {


            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.RECEIVE_SMS)) {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.RECEIVE_SMS},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }

        } else {

        }
    }
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.RECEIVE_SMS)
                            == PackageManager.PERMISSION_GRANTED) {
                    }

                } else {

                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }

            }

        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (intentFilter != null) {
            this.registerReceiver(readsms, intentFilter);
        }

    }

    @Override
    protected void onPause() {
        super.onPause();

        if (intentFilter != null) {
            this.unregisterReceiver(readsms);
        }
    }

}
