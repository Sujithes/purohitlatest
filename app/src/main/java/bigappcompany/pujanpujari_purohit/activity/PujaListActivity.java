package bigappcompany.pujanpujari_purohit.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.adapter.PujaListAdapter;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.common.ClsGeneral;
import bigappcompany.pujanpujari_purohit.login.Login;
import bigappcompany.pujanpujari_purohit.model.PujaListResultModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.DownlodableCallback;
import bigappcompany.pujanpujari_purohit.retrofitnetwork.RetrofitDataProvider;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by shankar on 8/3/18.
 */

public class PujaListActivity extends AppCompatActivity {

    @BindView(R.id.common_recyclerview)
    RecyclerView common_recyclerview;
    @BindView(R.id.mToolbar)
    Toolbar mToolbar;
    @BindView(R.id.button_submit)
    Button button_submit;

    ArrayList<PujaListResultModel> listModels = new ArrayList<>();
    ArrayList<String> list = new ArrayList<>();
    ArrayList<String> id = new ArrayList<>();
    private String canEdit;
    private int all_id[];
    private RetrofitDataProvider retrofitDataProvider;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.languagelist);
        ButterKnife.bind(this);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Celias.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        retrofitDataProvider = new RetrofitDataProvider(this);
        initWidgit();

    }

    @OnClick(R.id.button_submit)
    void doneClick(){
        if (canEdit.equals("puja")){
            all_id = new int[id.size()];
            for (int i=0; i<id.size();i++){
                all_id[i] = Integer.valueOf(id.get(i));
            }

            savePuja();
        }
        else {
            String message = "";
            for (int i = 0; i < list.size(); i++) {
                for (int j=0; j<listModels.size(); j++) {
                    if (list.get(i).equals(listModels.get(j).getPuja_name())) {
                        if (message.equals("")) {
                            message = listModels.get(j).getPuja_name();
                        } else {
                            message = message + "," + listModels.get(j).getPuja_name();

                        }
                    }
                }

            }
            Intent intent = new Intent();
            intent.putExtra("MESSAGE", message);
            intent.putStringArrayListExtra("ID", id);
            setResult(2, intent);
            finish();
        }
    }

    private void savePuja() {

        Utilz.showProgress(PujaListActivity.this, getResources().getString(R.string.save_language));
        String sessiontoken = ClsGeneral.getPreferences(PujaListActivity.this, PreferenceName.USER_SESSION_TOKEN);
        String auth = "Bearer "+ClsGeneral.getPreferences(PujaListActivity.this,PreferenceName.USER_TOKEN);
        retrofitDataProvider.savePujaService(sessiontoken, auth, all_id, new DownlodableCallback<StatusResultModel>() {
            @Override
            public void onSuccess(final StatusResultModel result) {
                Utilz.dismissProgress();
                if (result.getApiStatus().contains(PreferenceName.TRUE)) {
                    String message = "";
                    for (int i = 0; i < list.size(); i++) {
                        if (message.equals("")) {
                            message = listModels.get(i).getPuja_name();
                        } else {
                            message = message + "," + listModels.get(i).getPuja_name();

                        }

                    }
                    Intent intent = new Intent();
                    intent.putExtra("MESSAGE", message);
                    intent.putStringArrayListExtra("ID", id);
                    setResult(2, intent);
                    finish();
                }
                else
                {

                    Utilz.dismissProgress();
                }


            }

            @Override
            public void onFailure(String error) {
                Utilz.dismissProgress();
            }

            @Override
            public void onUnauthorized(int errorNumber) {

                ClsGeneral.setPreferences(PujaListActivity.this, PreferenceName.USER_ID, "");
                startActivity(new Intent(PujaListActivity.this, Login.class));
                finish();
            }
        });
    }

    private void initWidgit() {
        listModels.clear();
        id.clear();
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        mToolbar.setTitle(getResources().getString(R.string.select_puja_service));
        button_submit.setText(getResources().getString(R.string.done));
        listModels = getIntent().getParcelableArrayListExtra("list");
        id = getIntent().getStringArrayListExtra("Id");
        canEdit = getIntent().getStringExtra("edit");
        if (id.size()>0) {
            setlanguageBasedOnId();
        }
        checkListSize();
        common_recyclerview.setLayoutManager(new LinearLayoutManager(this));
        common_recyclerview.setAdapter(new PujaListAdapter(PujaListActivity.this, listModels, id,R.layout.languagelist_row, new OnClickListener() {
            @Override
            public void onClick(int pos) {

            }

            @Override
            public void twoParameter(String text1, String text2, int pos) {
                if (text2.equalsIgnoreCase("add")){
                    list.add(text1);
                    id.add(listModels.get(pos).getId());
                }
                else {
                    for (int i=0;i<list.size(); i++){
                        if (list.get(i).equals(text1)){
                            list.remove(i);
                            id.remove(i);
                        }
                    }
                }

                checkListSize();

            }
        }));

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void checkListSize() {
        if (list.size()>0){
            button_submit.setVisibility(View.VISIBLE);
        }
        else {
            button_submit.setVisibility(View.GONE);
        }
    }

    private void setlanguageBasedOnId() {
        list.clear();
        for (int i=0;i<listModels.size();i++) {
            for (int j = 0; j < id.size(); j++) {
                if (listModels.get(i).getId().equals(id.get(j))){
                    list.add(listModels.get(i).getPuja_name());
                }
            }
        }
    }
}
