// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.activity;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.customdatepicker.CalendarView;
import butterknife.Unbinder;
import butterknife.internal.DebouncingOnClickListener;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class CalendarActivity_ViewBinding implements Unbinder {
  private CalendarActivity target;

  private View view2131296323;

  @UiThread
  public CalendarActivity_ViewBinding(CalendarActivity target) {
    this(target, target.getWindow().getDecorView());
  }

  @UiThread
  public CalendarActivity_ViewBinding(final CalendarActivity target, View source) {
    this.target = target;

    View view;
    target.cv = Utils.findRequiredViewAsType(source, R.id.calendar_view, "field 'cv'", CalendarView.class);
    target.rv_timeslot = Utils.findRequiredViewAsType(source, R.id.rv_timeslot, "field 'rv_timeslot'", RecyclerView.class);
    target.mToolbar = Utils.findRequiredViewAsType(source, R.id.mToolbar, "field 'mToolbar'", Toolbar.class);
    target.wholedate = Utils.findRequiredViewAsType(source, R.id.wholedate, "field 'wholedate'", CheckBox.class);
    view = Utils.findRequiredView(source, R.id.button_submit, "field 'button_submit' and method 'onButtonClick'");
    target.button_submit = Utils.castView(view, R.id.button_submit, "field 'button_submit'", Button.class);
    view2131296323 = view;
    view.setOnClickListener(new DebouncingOnClickListener() {
      @Override
      public void doClick(View p0) {
        target.onButtonClick();
      }
    });
  }

  @Override
  @CallSuper
  public void unbind() {
    CalendarActivity target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.cv = null;
    target.rv_timeslot = null;
    target.mToolbar = null;
    target.wholedate = null;
    target.button_submit = null;

    view2131296323.setOnClickListener(null);
    view2131296323 = null;
  }
}
