package bigappcompany.pujanpujari_purohit.retrofitnetwork;


import bigappcompany.pujanpujari_purohit.common.ConsantValue;
import bigappcompany.pujanpujari_purohit.model.LanguageListModel;
import bigappcompany.pujanpujari_purohit.model.PujaListModel;
import bigappcompany.pujanpujari_purohit.model.PurohitDetailsModel;
import bigappcompany.pujanpujari_purohit.model.PurohitSlotAvalabilityModel;
import bigappcompany.pujanpujari_purohit.model.SignUpModel;
import bigappcompany.pujanpujari_purohit.model.StatusResultModel;
import bigappcompany.pujanpujari_purohit.model.TimeSlotModel;
import bigappcompany.pujanpujari_purohit.model.UserBookingListModel;
import bigappcompany.pujanpujari_purohit.model.UserDetailsModel;
import bigappcompany.pujanpujari_purohit.utilz.PreferenceName;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiRetrofitService {

    @POST(ConsantValue.LOGINWITHMOBILENUMBER)
    @FormUrlEncoded
    Call<UserDetailsModel> otpLogin(@Header(PreferenceName.APIKEY) String apikey, @Field("mobile_number") String mobile_no, @Field("role") String role);

    @POST(ConsantValue.VERIFYOTP)
    @FormUrlEncoded
    Call<UserDetailsModel> otpVerification(@Header(PreferenceName.APIKEY) String apikey,@Field("mobile_number") String mobile_no,@Field("otp") String otp, @Field("role") String role, @Field("device_token") String device_token);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.LISTLANGUAGE)
    Call<LanguageListModel> getallLanguage(@Header(PreferenceName.SESSION_TOKEN_HEADER) String header, @Header(PreferenceName.AUTHORIZATION) String authorization);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.SIGHNUP)
    @FormUrlEncoded
    Call<SignUpModel> signUp(@Field("first_name") String first_name, @Field("last_name") String last_name, @Field("role") String role,
                             @Field("email") String email, @Field("mobile_number") String mobile_number, @Field("address") String address, @Field("latitude") String latitude,
                             @Field("longitude") String longitude, @Field("area_of_service") String area_of_service);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.ADDPUROHITLANGUAGE)
    @FormUrlEncoded
    Call<StatusResultModel> addlanguage(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("language_id") int[] language_id);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.PUJALISTFORPUROHIT)
    Call<PujaListModel> getPujaList(@Header(PreferenceName.SESSION_TOKEN_HEADER) String header, @Header(PreferenceName.AUTHORIZATION) String authorization);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.ADDPUROHITPUJA)
    @FormUrlEncoded
    Call<StatusResultModel> addPujaService(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("puja_id") int[] puja_id);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.PUROHTDETAILS)
    Call<PurohitDetailsModel> getPurohitDetails(@Header(PreferenceName.SESSION_TOKEN_HEADER) String header, @Header(PreferenceName.AUTHORIZATION) String authorization);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.PUROHITTIMESLOT)
    Call<TimeSlotModel> purohitTimeSlot(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.SLOTAVAILABLE)
    @FormUrlEncoded
    Call<PurohitSlotAvalabilityModel> slotAvailable(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("date") String date);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.SETTIMESLOT)
    @FormUrlEncoded
    Call<StatusResultModel> addTimeslot(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("slot_id") int[] slot_id, @Field("date") String date);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.UPDATEPUROHITDETAILS)
    @FormUrlEncoded
    Call<StatusResultModel> addpurohitDetails(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("area_of_service") String area_of_service);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.ACCEPTBOOKING)
    @FormUrlEncoded
    Call<StatusResultModel> acceptBooking(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("request_id") String area_of_service);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.REJECTBOOKING)
    @FormUrlEncoded
    Call<StatusResultModel> rejectBooking(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("request_id") String area_of_service);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.USERBOOKINGLIST)
    Call<UserBookingListModel> userBookingList(@Header(PreferenceName.SESSION_TOKEN_HEADER) String header, @Header(PreferenceName.AUTHORIZATION) String authorization, @Query("request_status") String request_status);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @GET(ConsantValue.USERBOOKEDLIST)
    Call<UserBookingListModel> userBookedToCompleted(@Header(PreferenceName.SESSION_TOKEN_HEADER) String header, @Header(PreferenceName.AUTHORIZATION) String authorization, @Query("booking_status") String booking_status);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.UPDATEBOOKING)
    @FormUrlEncoded
    Call<StatusResultModel> updateBooking(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("booking_id") String booking_id, @Field("booking_status") String booking_status);

    @Headers(PreferenceName.APIKEY+":"+ ConsantValue.API_KEY)
    @POST(ConsantValue.UPDATEPUROHITDETAILS)
    @FormUrlEncoded
    Call<StatusResultModel> addpurohitAddressDetails(@Header(PreferenceName.SESSION_TOKEN_HEADER) String headertoken, @Header(PreferenceName.AUTHORIZATION) String authorization, @Field("address") String address, @Field("latitude") String latitude, @Field("longitude") String longitude);

}
