// Generated code from Butter Knife. Do not modify!
package bigappcompany.pujanpujari_purohit.adapter;

import android.support.annotation.CallSuper;
import android.support.annotation.UiThread;
import android.view.View;
import android.widget.CheckBox;
import bigappcompany.pujanpujari_purohit.R;
import butterknife.Unbinder;
import butterknife.internal.Utils;
import java.lang.IllegalStateException;
import java.lang.Override;

public class PujaListAdapter$MyViewHolder_ViewBinding implements Unbinder {
  private PujaListAdapter.MyViewHolder target;

  @UiThread
  public PujaListAdapter$MyViewHolder_ViewBinding(PujaListAdapter.MyViewHolder target,
      View source) {
    this.target = target;

    target.languageCheck = Utils.findRequiredViewAsType(source, R.id.languageCheck, "field 'languageCheck'", CheckBox.class);
  }

  @Override
  @CallSuper
  public void unbind() {
    PujaListAdapter.MyViewHolder target = this.target;
    if (target == null) throw new IllegalStateException("Bindings already cleared.");
    this.target = null;

    target.languageCheck = null;
  }
}
