package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 8/3/18.
 */

public class PujaListModel {
    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public ArrayList<PujaListResultModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<PujaListResultModel> result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    ArrayList<PujaListResultModel> result;
}
