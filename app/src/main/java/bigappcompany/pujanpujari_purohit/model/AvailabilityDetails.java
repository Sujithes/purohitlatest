package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by shankar on 12/3/18.
 */

public class AvailabilityDetails {


    @SerializedName("availability_id")
    String availability_id;
    @SerializedName("slot_id")
    String slot_id;
    @SerializedName("available_type")
    String available_type;
    @SerializedName("id")
    String id;

    public String getAvailability_id() {
        return availability_id;
    }

    public void setAvailability_id(String availability_id) {
        this.availability_id = availability_id;
    }

    public String getSlot_id() {
        return slot_id;
    }

    public void setSlot_id(String slot_id) {
        this.slot_id = slot_id;
    }

    public String getAvailable_type() {
        return available_type;
    }

    public void setAvailable_type(String available_type) {
        this.available_type = available_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    private boolean selected;

    public AvailabilityDetails(String available_type, boolean selected, String id){

        this.available_type = available_type;
        this.selected = selected;
        this.id = id;
    }
}
