package bigappcompany.pujanpujari_purohit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import bigappcompany.pujanpujari_purohit.R;
import bigappcompany.pujanpujari_purohit.allinterface.OnClickListener;
import bigappcompany.pujanpujari_purohit.model.UserBookingListResult;
import bigappcompany.pujanpujari_purohit.utilz.Utilz;
import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by shankar on 20/3/18.
 */

public class NewRequestAdapter extends RecyclerView.Adapter<NewRequestAdapter.MyViewHolder> {

    private List<UserBookingListResult> bookingList;
    private Context context;
    private int categorylist_row;
    private OnClickListener onClickListener ;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_userImage)
        CircularImageView iv_userImage;
        @BindView(R.id.tv_userName)
        TextView tv_userName;
        @BindView(R.id.tv_contact)
        TextView tv_contact;
        @BindView(R.id.tv_orderIdvalue)
        TextView tv_orderIdvalue;
        @BindView(R.id.iv_product)
        CircularImageView iv_product;
        @BindView(R.id.tv_productname)
        TextView tv_productname;
        @BindView(R.id.tv_date)
        TextView tv_date;
        @BindView(R.id.tv_time)
        TextView tv_time;
        @BindView(R.id.tv_languageName)
        TextView tv_languageName;

        public MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this,view);
        }
    }


    public NewRequestAdapter(Context context, ArrayList<UserBookingListResult> bookingList, int categorylist_row, OnClickListener onClickListener) {
        this.context = context;
        this.bookingList = bookingList;
        this.categorylist_row = categorylist_row;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(categorylist_row, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final int pos = position;
        final UserBookingListResult pl = bookingList.get(position);

        //Glide.with(context).load(pl.getUser_details().getProfile_image()).into(holder.iv_userImage);
        Picasso.with(context).load(pl.getUser_details().getProfile_image()).placeholder(R.drawable.default_icon).into(holder.iv_product);

//        Glide.with(context).load(pl.getTime_slot().get(0).get).into(holder.iv_product);
       try {
           String ln = pl.getUser_details().getLast_name();
           if (ln.equals("null") || ln.equals(null)) {
               holder.tv_userName.setText(pl.getUser_details().getFirst_name());
           } else {
               holder.tv_userName.setText(pl.getUser_details().getFirst_name() + " " + pl.getUser_details().getLast_name());
           }
       }
       catch (Exception e){
           holder.tv_userName.setText(pl.getUser_details().getFirst_name());
       }

        holder.tv_contact.setText(pl.getUser_details().getMobile_number());
        holder.tv_orderIdvalue.setText(pl.getId());
        holder.tv_productname.setText(pl.getPurohit_puja());
        holder.tv_date.setText(Utilz.dateFormat(pl.getBooking_date(), 10));
        holder.tv_languageName.setText(pl.getPurohit_language());
        try {
            if (pl.getTime_slot().size() > 0) {

                holder.tv_time.setText(Utilz.getFromdate(pl.getTime_slot().get(0).getSlot_id().getFrom_time()) + " " + Utilz.getToTime(pl.getTime_slot().get(0).getSlot_id().getTo_time()));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickListener.onClick(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }
}


