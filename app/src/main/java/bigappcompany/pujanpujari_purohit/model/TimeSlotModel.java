package bigappcompany.pujanpujari_purohit.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by shankar on 12/3/18.
 */

public class TimeSlotModel {

    public String getApiStatus() {
        return apiStatus;
    }

    public void setApiStatus(String apiStatus) {
        this.apiStatus = apiStatus;
    }

    public ArrayList<TimeSlotResultModel> getResult() {
        return result;
    }

    public void setResult(ArrayList<TimeSlotResultModel> result) {
        this.result = result;
    }

    @SerializedName("apiStatus")
    String apiStatus;
    @SerializedName("result")
    ArrayList<TimeSlotResultModel> result;
}
